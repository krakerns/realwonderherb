<?php

use yii\helpers\Html;
//use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\Roles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="roles-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'discount',['addon'=>['append' => ['content'=>'%']]])->textInput() ?>

    <?= $form->field($model, 'marketing_support',['addon'=>['append' => ['content'=>'%']]])->textInput() ?>

    <?= $form->field($model, 'referral',['addon'=>['append' => ['content'=>'%']]])->textInput() ?>

    <?= $form->field($model, 'rebates',['addon'=>['append' => ['content'=>'%']]])->textInput() ?>

    <?= $form->field($model, 'term_id')->dropDownList(Yii::$app->LibraryLoader->getTermsArray('-Select Terms-'),['id'=>'term_id'])->label('Terms') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
