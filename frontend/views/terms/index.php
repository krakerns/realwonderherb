<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PaymentTermsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Payment Terms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-terms-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Payment Terms', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           
           
            'description',

            ['class' => 'yii\grid\ActionColumn','header'=>'Action','headerOptions' => ['width' => '80'],],
        ],
        'tableOptions' =>['class' => 'table table-striped table-bordered'],

    ]); ?>
</div>
