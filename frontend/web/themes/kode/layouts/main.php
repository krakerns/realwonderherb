<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>

<?php $this->beginPage() ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= Html::encode($this->title) ?></title>

        <!-- ========== Css Files ========== -->
        <link href="<?php echo Yii::getAlias('@web'); ?>/themes/kode/css/root.css" rel="stylesheet">
        <link href="<?php echo Yii::getAlias('@web'); ?>/themes/kode/css/custom.css" rel="stylesheet">
        <link href="<?php echo Yii::getAlias('@web'); ?>/themes/kode/css/parsley.css" rel="stylesheet">
        <link href="<?php echo Yii::getAlias('@web'); ?>/themes/kode/js/dropzone/css/dropzone.css" rel="stylesheet">
        <?= Html::csrfMetaTags() ?>

        <style>

            .height-max{
                min-height: 78vh;
            }

            .content{
                padding-top:58px !important;
            }

        </style>

    </head>t

    <body>
    <?php $this->beginBody() ?>


    
    <!-- Start Page Loading -->
    <div class="loading"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/loading.gif" alt="loading-img"></div>
    <!-- End Page Loading -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START TOP -->
    <div id="top" class="clearfix">

        <!-- Start App Logo -->
        <div class="applogo">
            <a href="index.html" class="logo">Commission</a>
        </div>
        <!-- End App Logo -->

        <!-- Start Sidebar Show Hide Button -->
        <a href="#" class="sidebar-open-button"><i class="fa fa-bars"></i></a>
        <a href="#" class="sidebar-open-button-mobile"><i class="fa fa-bars"></i></a>
        <!-- End Sidebar Show Hide Button -->

        <!-- Start Searchbox -->
        <form class="searchform" style="display:none;">
            <input type="text" class="searchbox" id="searchbox" placeholder="Search">
            <span class="searchbutton"><i class="fa fa-search"></i></span>
        </form>
        <!-- End Searchbox -->

        <!-- Start Top Menu -->
        <ul class="topmenu" style="display: none;">
            <li><a href="#">Files</a></li>
            <li><a href="#">Authors</a></li>
            <li class="dropdown">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle">My Files <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><a href="#">Videos</a></li>
                    <li><a href="#">Pictures</a></li>
                    <li><a href="#">Blog Posts</a></li>
                </ul>
            </li>
        </ul>
        <!-- End Top Menu -->

       

        <!-- Start Top Right -->
        <ul class="top-right">

            <li class="dropdown link">
                <a href="#" data-toggle="dropdown" class="dropdown-toggle profilebox"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/profileimg4.png" alt="img"><b>JV Pogi</b><span class="caret"></span></a>
                <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right">
                    <li role="presentation" class="dropdown-header">Profile</li>
                    <li><a href="#"><i class="fa falist fa-inbox"></i>Inbox<span class="badge label-danger">4</span></a></li>
                    <li><a href="#"><i class="fa falist fa-file-o"></i>Files</a></li>
                    <li><a href="#"><i class="fa falist fa-wrench"></i>Settings</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="fa falist fa-lock"></i> Lockscreen</a></li>
                    <li><a href="#"><i class="fa falist fa-power-off"></i> Logout</a></li>
                </ul>
            </li>

        </ul>
        <!-- End Top Right -->

    </div>
    <!-- END TOP -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START SIDEBAR -->
    <div class="sidebar  clearfix">

        <ul class="sidebar-panel nav">
            <li class="sidetitle">MAIN</li>
            <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/site']); ?>"><span class="icon color5"><i class="fa fa-home"></i></span>Dashboard</a></li>
            <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/customers']); ?>"><span class="icon color5"><i class="fa fa-group"></i></span>Customers</a></li>
            <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/products']); ?>"><span class="icon color5"><i class="fa fa-pagelines"></i></span>Products</a></li>

            <li><a href="#"><span class="icon color9"><i class="fa fa-bars"></i></span>Orders<span class="caret"></span></a>
                <ul>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/project/list']); ?>">All</a></li>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/project/list/pending']); ?>">Pending</a></li>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/project/list/inprogress']); ?>">In Progress</a></li>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/project/list/complete']); ?>">Complete</a></li>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/project/list/archive']); ?>">Archive</a></li>
                </ul>
            </li>





            <li><a href="#"><span class="icon color8"><i class="fa fa-bar-chart-o"></i></span>Finance<span class="caret"></span></a>
                <ul>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/finance/quote']); ?>">Quotes</a></li>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/finance/moneyin']); ?>">Money In</a></li>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/finance/moneyout']); ?>">Money Out</a></li>
                </ul>
            </li>





        </ul>

        <ul class="sidebar-panel nav">
            <li class="sidetitle">MORE</li>
            <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/finance/quote/newquote']); ?>"><span class="icon color15"><i class="fa fa-pencil-square-o"></i></span>New Order</a></li>
            <li><a href="#"><span class="icon color11"><i class="fa fa-cogs"></i></span>Settings<span class="caret"></span></a>
                <ul>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/roles']); ?>">Roles</a></li>
                    <li><a href="<?php echo Yii::$app->urlManager->createUrl(['/terms']); ?>">Terms</a></li>
                </ul>
            </li>
        </ul>

    </div>
    <!-- END SIDEBAR -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START CONTENT -->
    <div class="content" >


            <div style="min-height: 700px !important">
            <?= $content ?>
            </div>

            <!-- Start Footer -->
            <div class="row footer">
                <div class="col-md-6 text-left">
                    Copyright © 2015 <a href="http://themeforest.net/user/egemem/portfolio" target="_blank">Egemem</a> All rights reserved.
                </div>
                <div class="col-md-6 text-right">
                    Design and Developed by <a href="http://themeforest.net/user/egemem/portfolio" target="_blank">Egemem</a>
                </div>
            </div>
            <!-- End Footer -->


        </div>
    <!-- End Content -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->


    <!-- //////////////////////////////////////////////////////////////////////////// -->
    <!-- START SIDEPANEL -->
    <div role="tabpanel" class="sidepanel">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#today" aria-controls="today" role="tab" data-toggle="tab">TODAY</a></li>
            <li role="presentation"><a href="#tasks" aria-controls="tasks" role="tab" data-toggle="tab">TASKS</a></li>
            <li role="presentation"><a href="#chat" aria-controls="chat" role="tab" data-toggle="tab">CHAT</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">

            <!-- Start Today -->
            <div role="tabpanel" class="tab-pane active" id="today">

                <div class="sidepanel-m-title">
                    Today
                    <span class="left-icon"><a href="#"><i class="fa fa-refresh"></i></a></span>
                    <span class="right-icon"><a href="#"><i class="fa fa-file-o"></i></a></span>
                </div>

                <div class="gn-title">NEW</div>

                <ul class="list-w-title">
                    <li>
                        <a href="#">
                            <span class="label label-danger">ORDER</span>
                            <span class="date">9 hours ago</span>
                            <h4>New Jacket 2.0</h4>
                            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label label-success">COMMENT</span>
                            <span class="date">14 hours ago</span>
                            <h4>Bill Jackson</h4>
                            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label label-info">MEETING</span>
                            <span class="date">at 2:30 PM</span>
                            <h4>Developer Team</h4>
                            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <span class="label label-warning">EVENT</span>
                            <span class="date">3 days left</span>
                            <h4>Birthday Party</h4>
                            Etiam auctor porta augue sit amet facilisis. Sed libero nisi, scelerisque.
                        </a>
                    </li>
                </ul>

            </div>
            <!-- End Today -->

            <!-- Start Tasks -->
            <div role="tabpanel" class="tab-pane" id="tasks">

                <div class="sidepanel-m-title">
                    To-do List
                    <span class="left-icon"><a href="#"><i class="fa fa-pencil"></i></a></span>
                    <span class="right-icon"><a href="#"><i class="fa fa-trash"></i></a></span>
                </div>

                <div class="gn-title">TODAY</div>

                <ul class="todo-list">
                    <li class="checkbox checkbox-primary">
                        <input id="checkboxside1" type="checkbox"><label for="checkboxside1">Add new products</label>
                    </li>

                    <li class="checkbox checkbox-primary">
                        <input id="checkboxside2" type="checkbox"><label for="checkboxside2"><b>May 12, 6:30 pm</b> Meeting with Team</label>
                    </li>

                    <li class="checkbox checkbox-warning">
                        <input id="checkboxside3" type="checkbox"><label for="checkboxside3">Design Facebook page</label>
                    </li>

                    <li class="checkbox checkbox-info">
                        <input id="checkboxside4" type="checkbox"><label for="checkboxside4">Send Invoice to customers</label>
                    </li>

                    <li class="checkbox checkbox-danger">
                        <input id="checkboxside5" type="checkbox"><label for="checkboxside5">Meeting with developer team</label>
                    </li>
                </ul>

                <div class="gn-title">TOMORROW</div>
                <ul class="todo-list">
                    <li class="checkbox checkbox-warning">
                        <input id="checkboxside6" type="checkbox"><label for="checkboxside6">Redesign our company blog</label>
                    </li>

                    <li class="checkbox checkbox-success">
                        <input id="checkboxside7" type="checkbox"><label for="checkboxside7">Finish client work</label>
                    </li>

                    <li class="checkbox checkbox-info">
                        <input id="checkboxside8" type="checkbox"><label for="checkboxside8">Call Johnny from Developer Team</label>
                    </li>

                </ul>
            </div>
            <!-- End Tasks -->

            <!-- Start Chat -->
            <div role="tabpanel" class="tab-pane" id="chat">

                <div class="sidepanel-m-title">
                    Friend List
                    <span class="left-icon"><a href="#"><i class="fa fa-pencil"></i></a></span>
                    <span class="right-icon"><a href="#"><i class="fa fa-trash"></i></a></span>
                </div>

                <div class="gn-title">ONLINE MEMBERS (3)</div>
                <ul class="group">
                    <li class="member"><a href="#"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/profileimg.png" alt="img"><b>Allice Mingham</b>Los Angeles</a><span class="status online"></span></li>
                    <li class="member"><a href="#"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/profileimg2.png" alt="img"><b>James Throwing</b>Las Vegas</a><span class="status busy"></span></li>
                    <li class="member"><a href="#"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/profileimg3.png" alt="img"><b>Fred Stonefield</b>New York</a><span class="status away"></span></li>
                    <li class="member"><a href="#"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/profileimg4.png" alt="img"><b>Chris M. Johnson</b>California</a><span class="status online"></span></li>
                </ul>

                <div class="gn-title">OFFLINE MEMBERS (8)</div>
                <ul class="group">
                    <li class="member"><a href="#"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/profileimg5.png" alt="img"><b>Allice Mingham</b>Los Angeles</a><span class="status offline"></span></li>
                    <li class="member"><a href="#"><img src="<?php echo Yii::getAlias('@web'); ?>/themes/kode/img/profileimg6.png" alt="img"><b>James Throwing</b>Las Vegas</a><span class="status offline"></span></li>
                </ul>

                <form class="search">
                    <input type="text" class="form-control" placeholder="Search a Friend...">
                </form>
            </div>
            <!-- End Chat -->

        </div>

    </div>
    <!-- END SIDEPANEL -->
    <!-- //////////////////////////////////////////////////////////////////////////// -->

    <?php $this->endBody() ?>
    </body>

</html>
<?php $this->endPage() ?>
