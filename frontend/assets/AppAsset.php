<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        '/themes/kode/js/bootstrap/bootstrap.min.js',
        '/themes/kode/js/plugins.js',
        '/themes/kode/js/parsley.js',
        '/themes/kode/js/fileinput.js',
        '/themes/kode/js/moment/moment.min.js',
        '/themes/kode/js/date-range-picker/daterangepicker.js',
        '/themes/kode/js/bootstrap-select/bootstrap-select.js',
        '/themes/kode/js/bootstrap-toggle/bootstrap-toggle.min.js',
        '/themes/kode/js/bootstrap-wysihtml5/bootstrap-wysihtml5.js',
        '/themes/kode/js/bootstrap-wysihtml5/wysihtml5-0.3.0.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
