<?php 

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use common\models;

use yii\helpers\ArrayHelper;

class LibraryLoader extends Component{

	public function welcome()
    {
        echo "Hello..Welcome to MyComponent";
    }

    public function getProductTypeArray($selectOption=''){
        $productTypes = models\ProductType::find()->all();
        $arr =  ArrayHelper::map($productTypes, 'id', 'name');
        return array('' => $selectOption) + $arr;
    }

    public function getTermsArray($selectOption=''){
        $terms = models\PaymentTerms::find()->all();
        $arr =  ArrayHelper::map($terms, 'id', 'description');
        return array('' => $selectOption) + $arr;
    }

    public function getCities($selectOption=''){
        $items = models\Cities::find()->all();
        $arr =  ArrayHelper::map($items, 'id', 'name');
        return array('' => $selectOption) + $arr;
    }

    public function getSponsors($selectOption=''){
        $sponsors = models\Customers::find()->all();
        $arr =  ArrayHelper::map($sponsors, 'id', function($model, $defaultValue) {
                return $model->first_name.' '.$model->last_name;
            });
        return array('' => $selectOption) + $arr;
    }

    public function getCategories($selectOption=''){
        $items = models\Category::find()->all();
        $arr =  ArrayHelper::map($items, 'id', 'cat_name');
        return array('' => $selectOption) + $arr;
    }

    function leading_zeros($value, $places){
    // Function written by Marcus L. Griswold (vujsa)
    // Can be found at http://www.handyphp.com
    // Do not remove this header!
        $leading= "";
        if(is_numeric($value)){
            for($x = 1; $x <= $places; $x++){
                $ceiling = pow(10, $x);
                if($value < $ceiling){
                    $zeros = $places - $x;
                    for($y = 1; $y <= $zeros; $y++){
                        $leading .= "0";
                    }
                    $x = $places + 1;
                }
            }
            $output = $leading . $value;
        }
        else{
            $output = $value;
        }
        return $output;
    }
}

?>