<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "roles".
 *
 * @property integer $id
 * @property string $name
 * @property double $discount
 * @property double $marketing_support
 * @property double $referral
 * @property double $rebates
 * @property integer $term_id
 *
 * @property PaymentTerms $term
 */
class Roles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discount', 'marketing_support', 'referral', 'rebates'], 'number'],
            [['term_id'], 'integer'],
            [['name'], 'string', 'max' => 36],
            [['term_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentTerms::className(), 'targetAttribute' => ['term_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'discount' => 'Discount',
            'marketing_support' => 'Marketing Support',
            'referral' => 'Referral',
            'rebates' => 'Rebates',
            'term_id' => 'Term ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerm()
    {
        return $this->hasOne(PaymentTerms::className(), ['id' => 'term_id']);
    }
}
