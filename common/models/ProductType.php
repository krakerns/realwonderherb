<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_type".
 *
 * @property string $id
 * @property string $name
 * @property string $define_name
 * @property string $description
 */
class ProductType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'define_name', 'description'], 'required'],
            [['name', 'define_name', 'description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'define_name' => 'Define Name',
            'description' => 'Description',
        ];
    }
}
