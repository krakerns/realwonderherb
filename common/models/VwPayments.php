<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vw_payments".
 *
 * @property integer $order_id
 * @property double $amount_paid
 * @property string $bank_name
 * @property string $bank_number
 * @property string $cheque_number
 * @property string $payment_type
 * @property double $total_amount
 * @property integer $id
 * @property integer $customer_id
 * @property integer $sponsor_id
 * @property double $grand_total
 * @property double $mini_subsidy
 * @property double $discount
 * @property double $amount_due
 * @property integer $created_by
 * @property string $created
 * @property double $sub_total
 * @property double $mini_subsidy_perc
 * @property double $mini_accumulated_sales
 * @property string $customer_name
 * @property string $sponsor_name
 */
class VwPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vw_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'id', 'customer_id', 'sponsor_id', 'created_by'], 'integer'],
            [['amount_paid', 'total_amount', 'grand_total', 'mini_subsidy', 'discount', 'amount_due', 'sub_total', 'mini_subsidy_perc', 'mini_accumulated_sales'], 'number'],
            [['created'], 'safe'],
            [['bank_name', 'bank_number', 'cheque_number'], 'string', 'max' => 36],
            [['payment_type'], 'string', 'max' => 20],
            [['customer_name', 'sponsor_name'], 'string', 'max' => 201],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'amount_paid' => 'Amount Paid',
            'bank_name' => 'Bank Name',
            'bank_number' => 'Bank Number',
            'cheque_number' => 'Cheque Number',
            'payment_type' => 'Payment Type',
            'total_amount' => 'Total Amount',
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'sponsor_id' => 'Sponsor ID',
            'grand_total' => 'Grand Total',
            'mini_subsidy' => 'Mini Subsidy',
            'discount' => 'Discount',
            'amount_due' => 'Amount Due',
            'created_by' => 'Created By',
            'created' => 'Created',
            'sub_total' => 'Sub Total',
            'mini_subsidy_perc' => 'Mini Subsidy Perc',
            'mini_accumulated_sales' => 'Mini Accumulated Sales',
            'customer_name' => 'Customer Name',
            'sponsor_name' => 'Sponsor Name',
        ];
    }
}
