<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vw_sales_invoices".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $sponsor_id
 * @property double $grand_total
 * @property double $mini_subsidy
 * @property double $discount
 * @property double $amount_due
 * @property integer $created_by
 * @property string $created
 * @property double $sub_total
 * @property double $mini_subsidy_perc
 * @property double $mini_accumulated_sales
 * @property string $customer_name
 * @property string $sponsor_name
 */
class VwSalesInvoices extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vw_sales_invoices';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'sponsor_id', 'created_by'], 'integer'],
            [['grand_total', 'mini_subsidy', 'discount', 'amount_due', 'sub_total', 'mini_subsidy_perc', 'mini_accumulated_sales'], 'number'],
            [['created'], 'safe'],
            [['customer_name', 'sponsor_name'], 'string', 'max' => 201],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'sponsor_id' => 'Sponsor ID',
            'grand_total' => 'Grand Total',
            'mini_subsidy' => 'Mini Subsidy',
            'discount' => 'Discount',
            'amount_due' => 'Amount Due',
            'created_by' => 'Created By',
            'created' => 'Created',
            'sub_total' => 'Sub Total',
            'mini_subsidy_perc' => 'Mini Subsidy Perc',
            'mini_accumulated_sales' => 'Mini Accumulated Sales',
            'customer_name' => 'Customer Name',
            'sponsor_name' => 'Sponsor Name',
        ];
    }
}
