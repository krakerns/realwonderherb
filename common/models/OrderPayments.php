<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order_payments".
 *
 * @property integer $id
 * @property integer $order_id
 * @property double $total_amount
 * @property double $amount_paid
 * @property double $change
 * @property string $payment_type
 * @property string $bank_name
 * @property string $bank_number
 * @property string $cheque_number
 * @property integer $is_invoice
 * @property string $date_paid
 * @property Orders $order
 */
class OrderPayments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'is_invoice'], 'integer'],
            [['total_amount', 'amount_paid', 'change'], 'number'],
            [['payment_type'], 'string', 'max' => 20],
            [['date_paid'], 'safe'],
            [['bank_name', 'bank_number', 'cheque_number'], 'string', 'max' => 36],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'total_amount' => 'Total Amount',
            'amount_paid' => 'Amount Paid',
            'change' => 'Change',
            'payment_type' => 'Payment Type',
            'bank_name' => 'Bank Name',
            'bank_number' => 'Bank Number',
            'cheque_number' => 'Cheque Number',
            'is_invoice' => 'Is Invoice',
            'date_paid' => 'Date Paid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id' => 'order_id']);
    }
}
