<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use common\models\VwproductsInventory;
use yii\db\Query;

/**
 * CitiesSearch represents the model behind the search form about `common\models\Cities`.
 */
class VwproductsInventorySearch extends VwproductsInventory
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id'], 'integer'],
            [['quantity', 'previous_quantity'], 'number'],
            [['sku', 'name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)    {

        $this->load($params);
        $query = new Query();
        $query->select('*')
            ->from('vwproducts_inventory');

        $this->load($params);

        if($this->sku){
            $query->where('sku  LIKE "%' . $this->sku . '%" ');
        }
        if($this->name){
            $query->where('name  LIKE "%' . $this->name . '%" ');
        }

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->sql,
            'pagination' => [
                'pageSize' => 100
            ],
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        return $dataProvider;
    }
}
