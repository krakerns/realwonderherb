<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Products;

/**
 * ProductsSearch represents the model behind the search form about `common\models\Products`.
 */
class ProductsSearch extends Products
{
    /**
     * @inheritdoc
     */
    public $categoryName;

    public function rules()
    {
        return [
            [['id', 'active', 'product_type_id', 'is_bundle', 'category_id'], 'integer'],
            [['sku', 'name', 'description', 'long_description', 'image', 'reverse_image', 'thumbnail_image','categoryName'], 'safe'],
            [['wholesale_price', 'retail_price', 'volume', 'weight', 'reorder_number'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Products::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'categoryName' => [
                    'asc' => ['category.name' => SORT_ASC],
                    'desc' => ['category.name' => SORT_DESC],
                    'label' => 'Category Name'
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            /**
            * The following line will allow eager loading with country data 
            * to enable sorting by country on initial loading of the grid.
            */ 
            $query->joinWith(['category']);
            return $dataProvider;
        }

        // $this->load($params);

        // if (!$this->validate()) {
        //     // uncomment the following line if you do not want to return any records when validation fails
        //     // $query->where('0=1');
        //     return $dataProvider;
        // }

        $query->andFilterWhere([
            'id' => $this->id,
            'wholesale_price' => $this->wholesale_price,
            'retail_price' => $this->retail_price,
            'volume' => $this->volume,
            'weight' => $this->weight,
            'active' => $this->active,
            'product_type_id' => $this->product_type_id,
            'is_bundle' => $this->is_bundle,
            'reorder_number' => $this->reorder_number,
            'category_id' => $this->category_id,
        ]);

        $query->andFilterWhere(['like', 'sku', $this->sku])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'long_description', $this->long_description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'reverse_image', $this->reverse_image])
            ->andFilterWhere(['like', 'category_id', $this->category_id])
            ->andFilterWhere(['like', 'thumbnail_image', $this->thumbnail_image]);

            // filter by category name
        $query->joinWith(['category' => function ($q) {
            $q->where('category.cat_name LIKE "%' . $this->categoryName . '%"');
        }]);

        return $dataProvider;
    }
}
