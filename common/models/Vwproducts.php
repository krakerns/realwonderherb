<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vwproducts".
 *
 * @property integer $id
 * @property string $sku
 * @property string $name
 * @property string $description
 * @property string $long_description
 * @property string $wholesale_price
 * @property string $retail_price
 * @property string $volume
 * @property string $weight
 * @property string $image
 * @property string $reverse_image
 * @property string $thumbnail_image
 * @property integer $active
 * @property string $product_type_id
 * @property integer $is_bundle
 * @property double $reorder_number
 * @property integer $category_id
 * @property string $sku_name
 */
class Vwproducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwproducts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'active', 'product_type_id', 'is_bundle', 'category_id'], 'integer'],
            [['sku', 'name', 'description', 'long_description'], 'required'],
            [['description', 'long_description'], 'string'],
            [['wholesale_price', 'retail_price', 'volume', 'weight', 'reorder_number'], 'number'],
            [['sku'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 100],
            [['image', 'reverse_image', 'thumbnail_image'], 'string', 'max' => 255],
            [['sku_name'], 'string', 'max' => 148],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'name' => 'Name',
            'description' => 'Description',
            'long_description' => 'Long Description',
            'wholesale_price' => 'Wholesale Price',
            'retail_price' => 'Retail Price',
            'volume' => 'Volume',
            'weight' => 'Weight',
            'image' => 'Image',
            'reverse_image' => 'Reverse Image',
            'thumbnail_image' => 'Thumbnail Image',
            'active' => 'Active',
            'product_type_id' => 'Product Type ID',
            'is_bundle' => 'Is Bundle',
            'reorder_number' => 'Reorder Number',
            'category_id' => 'Category ID',
            'sku_name' => 'Sku Name',
        ];
    }
}
