<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "bundle_lines".
 *
 * @property integer $id
 * @property integer $bundle_id
 * @property integer $product_id
 * @property string $bundle_price
 *
 * @property Products $bundle
 * @property Products $product
 */
class BundleLines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bundle_lines';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bundle_id', 'product_id'], 'integer'],
            [['bundle_price'], 'number'],
            [['bundle_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['bundle_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bundle_id' => 'Bundle ID',
            'product_id' => 'Product ID',
            'bundle_price' => 'Bundle Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBundle()
    {
        return $this->hasOne(Products::className(), ['id' => 'bundle_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
