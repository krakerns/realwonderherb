<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property integer $id
 * @property string $sku
 * @property string $name
 * @property string $description
 * @property string $long_description
 * @property string $wholesale_price
 * @property string $retail_price
 * @property string $volume
 * @property string $weight
 * @property string $image
 * @property string $reverse_image
 * @property string $thumbnail_image
 * @property integer $active
 * @property string $product_type_id
 * @property integer $is_bundle
 * @property double $reorder_number
 * @property integer $category_id
 *
 * @property BundleLines[] $bundleLines
 * @property BundleLines[] $bundleLines0
 * @property OrderLines[] $orderLines
 * @property Category $category
 * @property StockOnHand[] $stockOnHands
 * @property StockOnHandHistory[] $stockOnHandHistories
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sku', 'name', 'description', 'long_description'], 'required'],
            [['description', 'long_description'], 'string'],
            [['wholesale_price', 'retail_price', 'volume', 'weight', 'reorder_number'], 'double'],
            [['active', 'product_type_id', 'is_bundle', 'category_id'], 'integer'],
            [['sku'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 100],
            [['image', 'reverse_image', 'thumbnail_image'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'name' => 'Name',
            'description' => 'Description',
            'long_description' => 'Long Description',
            'wholesale_price' => 'Wholesale Price',
            'retail_price' => 'Retail Price',
            'volume' => 'Volume',
            'weight' => 'Weight',
            'image' => 'Image',
            'reverse_image' => 'Reverse Image',
            'thumbnail_image' => 'Thumbnail Image',
            'active' => 'Active',
            'product_type_id' => 'Product Type ID',
            'is_bundle' => 'Is Bundle',
            'reorder_number' => 'Reorder Number',
            'category_id' => 'Category ID',
            'categoryName' => Yii::t('app', 'Category Name')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBundleLines()
    {
        return $this->hasMany(BundleLines::className(), ['bundle_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBundleLines0()
    {
        return $this->hasMany(BundleLines::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLines()
    {
        return $this->hasMany(OrderLines::className(), ['product_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    public function getCategoryName(){
        return $this->category->cat_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockOnHands()
    {
        return $this->hasMany(StockOnHand::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStockOnHandHistories()
    {
        return $this->hasMany(StockOnHandHistory::className(), ['product_id' => 'id']);
    }
}
