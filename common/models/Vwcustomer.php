<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vwcustomer".
 *
 * @property string $full_name
 * @property integer $id
 * @property string $sponsor_name
 * @property integer $sponsor_id
 */
class Vwcustomer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwcustomer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sponsor_id'], 'integer'],
            [['full_name', 'sponsor_name'], 'string', 'max' => 201],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'full_name' => 'Full Name',
            'id' => 'ID',
            'sponsor_name' => 'Sponsor Name',
            'sponsor_id' => 'Sponsor ID',
        ];
    }
}
