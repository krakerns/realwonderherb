<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "deposit".
 *
 * @property integer $id
 * @property string $type
 * @property string $account_number
 * @property string $cheque_number
 * @property string $bank_name
 * @property string $branch
 * @property string $collection_date
 * @property string $deposit_date
 * @property string $created
 * @property double $amount
 */
class Deposit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'deposit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collection_date', 'deposit_date', 'created'], 'safe'],
            [['deposit_date'], 'required'],
            [['amount'], 'number'],
            [['type'], 'string', 'max' => 20],
            [['account_number', 'cheque_number', 'bank_name', 'branch'], 'string', 'max' => 40],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'account_number' => 'Account Number',
            'cheque_number' => 'Cheque Number',
            'bank_name' => 'Bank Name',
            'branch' => 'Branch',
            'collection_date' => 'Collection Date',
            'deposit_date' => 'Deposit Date',
            'created' => 'Created',
            'amount' => 'Amount',
        ];
    }
}
