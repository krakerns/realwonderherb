<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $business_name
 * @property string $first_name
 * @property string $last_name
 * @property string $contact_number
 * @property string $email
 * @property string $street_address
 * @property integer $city
 * @property string $state
 * @property string $country
 * @property integer $zipcode
 * @property resource $tax_number
 * @property string $ssn
 * @property string $date_of_birth
 * @property integer $active
 * @property string $timestamp
 * @property integer $sponsor_id
 * @property string $photo_url
 * @property integer $is_mini
 * @property string $gender
 * @property double $credit_limit
 *
 * @property Cities $city0
 * @property Customers $sponsor
 * @property Customers[] $customers
 * @property Orders[] $orders
 * @property Orders[] $orders0
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }
    public $image;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg, gif, png'],
            [['first_name', 'last_name','sponsor_id','email'], 'required'],
            [['street_address', 'tax_number','photo_url'], 'string'],
            [['city', 'zipcode', 'active', 'sponsor_id', 'is_mini'], 'integer'],
            [['date_of_birth', 'timestamp'], 'safe'],
            [['username', 'password', 'business_name','gender'], 'string', 'max' => 20],
            [['credit_limit'], 'number'],
            [['first_name', 'last_name'], 'string', 'max' => 100],
            [['contact_number'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 200],
            [['email'], 'email'],
            [['state', 'country'], 'string', 'max' => 50],
            [['ssn'], 'string', 'max' => 10],
            [['city'], 'exist', 'skipOnError' => true, 'targetClass' => Cities::className(), 'targetAttribute' => ['city' => 'id']],
            [['sponsor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['sponsor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'business_name' => 'Business Name',
            'fullName' => Yii::t('app', 'Name'),
            'last_name' => 'Last Name',
            'contact_number' => 'Contact Number',
            'email' => 'Email',
            'street_address' => 'Street Address',
            'city' => 'City',
            'state' => 'State',
            'country' => 'Country',
            'zipcode' => 'Zipcode',
            'tax_number' => 'Tax Number',
            'ssn' => 'Ssn',
            'date_of_birth' => 'Date Of Birth',
            'active' => 'Active',
            'timestamp' => 'Timestamp',
            'sponsorName' => 'Sponsor',
            'photo_url' => 'Photo',
            'is_mini' => 'Is Mini',
            'gender' => 'Gender',
            'credit_limit' => 'Credit Limit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity0()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsor()
    {

        return $this->hasOne(Customers::className(), ['id' => 'sponsor_id']);
        // $sponsor =
        // echo '<pre>'; var_dump($sponsor); echo '</pre>';die();
        // return $sponsor['first_name'];
        
    }

    public function getSponsorName(){
        return $this->sponsor->fullName;
    }



    public function getFullName()
    {
        return $this->first_name . ' ' . $this->last_name;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['sponsor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Orders::className(), ['customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders0()
    {
        return $this->hasMany(Orders::className(), ['sponsor_id' => 'id']);
    }
}
