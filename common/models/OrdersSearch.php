<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Orders;

/**
 * OrdersSearch represents the model behind the search form about `common\models\Orders`.
 */
class OrdersSearch extends Orders
{
    /**
     * @inheritdoc
     */

    public $customerName;
    public $sponsorName;

    public function rules()
    {
        return [
            [['id', 'customer_id', 'sponsor_id', 'created_by'], 'integer'],
            [['grand_total', 'mini_subsidy', 'discount', 'amount_due', 'sub_total', 'mini_subsidy_perc', 'mini_accumulated_sales'], 'number'],
            [['created','customerName','sponsorName'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Orders::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith(['customer']);
            $query->joinWith(['sponsor']);
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'sponsor_id' => $this->sponsor_id,
            'grand_total' => $this->grand_total,
            'mini_subsidy' => $this->mini_subsidy,
            'discount' => $this->discount,
            'amount_due' => $this->amount_due,
            'created_by' => $this->created_by,
            'created' => $this->created,
            'sub_total' => $this->sub_total,
            'mini_subsidy_perc' => $this->mini_subsidy_perc,
            'mini_accumulated_sales' => $this->mini_accumulated_sales,
        ]);

        // filter by country name
        $query->joinWith(['customer' => function ($q) {
            $q->where('customers.first_name LIKE "%' . $this->customerName . '%"  OR customers.last_name LIKE "%' . $this->customerName . '%"' );
        }]);


        return $dataProvider;
    }
}
