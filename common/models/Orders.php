<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property integer $sponsor_id
 * @property double $grand_total
 * @property double $mini_subsidy
 * @property double $discount
 * @property double $amount_due
 * @property integer $created_by
 * @property string $created
 * @property double $sub_total
 * @property double $mini_subsidy_perc
 * @property double $mini_accumulated_sales
 * @property integer $is_invoice
 * @property integer $is_paid
 * @property integer $terms
 * @property double $prev_balance
 * @property double $remaining_balance
 * @property string $last_payment_date
 * @property string $due_date
 * @property OrderLines[] $orderLines
 * @property OrderPayments[] $orderPayments
 * @property Customers $customer
 * @property Customers $sponsor
 * @property User $createdBy
 *
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'sponsor_id', 'created_by', 'is_invoice', 'is_paid','terms'], 'integer'],
            [['grand_total', 'mini_subsidy', 'discount', 'amount_due', 'sub_total', 'mini_subsidy_perc', 'mini_accumulated_sales','prev_balance', 'remaining_balance'], 'number'],
            [['created','last_payment_date','last_payment_date'], 'safe'],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['sponsor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Customers::className(), 'targetAttribute' => ['sponsor_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'customer_id' => 'Customer ID',
            'sponsor_id' => 'Sponsor ID',
            'grand_total' => 'Grand Total',
            'mini_subsidy' => 'Mini Subsidy',
            'discount' => 'Discount',
            'amount_due' => 'Amount Due',
            'created_by' => 'Created By',
            'created' => 'Created',
            'sub_total' => 'Sub Total',
            'mini_subsidy_perc' => 'Mini Subsidy Perc',
            'mini_accumulated_sales' => 'Mini Accumulated Sales',
            'customerName' => Yii::t('app', 'Customer Name'),
            'sponsorName' => Yii::t('app', 'Sponsor Name'),

            'is_invoice' => 'Is Invoice',
            'is_paid' => 'Is Paid',
            'terms' => 'Terms',
            'prev_balance' => 'Prev Balance',
            'remaining_balance' => 'Remaining Balance',
            'last_payment_date' => 'Last Payment Date',
            'due_date' => 'Due Date',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderLines()
    {
        return $this->hasMany(OrderLines::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPayments()
    {
        return $this->hasMany(OrderPayments::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['id' => 'customer_id']);
    }

    /* Getter for customer name */
    public function getCustomerName() {
        return $this->customer->first_name . ' ' . $this->customer->last_name;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSponsor()
    {
        return $this->hasOne(Customers::className(), ['id' => 'sponsor_id']);
    }
    /* Getter for customer name */
    public function getSponsorName() {
        return $this->sponsor->first_name . ' ' . $this->sponsor->last_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
