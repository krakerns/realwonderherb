<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "vwproducts_inventory".
 *
 * @property integer $id
 * @property integer $product_id
 * @property double $quantity
 * @property double $previous_quantity
 * @property integer $pid
 * @property string $sku
 * @property string $name
 * @property string $description
 */
class VwproductsInventory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vwproducts_inventory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'product_id', 'pid'], 'integer'],
            [['quantity', 'previous_quantity'], 'number'],
            [['sku', 'name', 'description'], 'required'],
            [['description'], 'string'],
            [['sku'], 'string', 'max' => 45],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'previous_quantity' => 'Previous Quantity',
            'pid' => 'Pid',
            'sku' => 'Sku',
            'name' => 'Name',
            'description' => 'Description',
        ];
    }
}
