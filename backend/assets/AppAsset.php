<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/jquery-ui.css',
        'css/jquery.dataTables.min.css',
        'css/dataTables.bootstrap.min.css',
        'css/jquery.treetable.css',
        'css/jquery.treetable.theme.default.css',
    ];
    public $js = [
        'js/lodash.js',
        'js/moment.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
