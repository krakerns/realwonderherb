<?php

namespace backend\controllers;

class StockController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionStockissuance()
    {
        return $this->render('stockissuance');
    }

    public function actionStockin()
    {
        return $this->render('stockin');
    }

    public function actionStocktransfer()
    {
        return $this->render('stocktransfer');
    }

    public function actionStockadjustment()
    {
        return $this->render('stockadjustment');
    }
}
