<?php

namespace backend\controllers;

use yii\db\Query;
use common\models\Deposit;

class ReportsController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCollections(){
        return $this->render('collections');
    }

    public function actionSalesgroup(){
        return $this->render('salesgroup');
    }

    public function actionSalesdealer(){
        return $this->render('salesdealer');
    }

    public function actionGenealogy(){
        return $this->render('genealogy');
    }

    public function actionSignups(){
        return $this->render('signups');
    }

    public function actionPassiveincome(){
        return $this->render('passiveincome');
    }

    public function actionGetcollections(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];
        $payment_type = $_POST['payment_type'];

        $query->select('*')
            ->from('vw_payments')
            ->where(' date(created) between :start_date AND :end_date AND payment_type=:payment_type',[':payment_type'=>$payment_type,':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }
    public function actionGetgroups(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];
        $sponsor_id = $_POST['sponsor_id'];

        $query->select('*')
            ->from('vw_payments')
            ->where(' date(created) between :start_date AND :end_date AND sponsor_id=:sponsor_id',[':sponsor_id'=>$sponsor_id,':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }

    public function actionGetdealers(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];
        $sponsor_id = $_POST['sponsor_id'];

        $query->select('*')
            ->from('vw_payments')
            ->where(' date(created) between :start_date AND :end_date AND customer_id=:sponsor_id',[':sponsor_id'=>$sponsor_id,':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }

    public function actionGetbranch(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];

        $query->select('*')
            ->from('vw_sales_invoices')
            ->where(' date(created) between :start_date AND :end_date ',[':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }

    public function actionGetcustomers(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];
        $sponsor_id = $_POST['sponsor_id'];

        $query->select('*')
            ->from('vwcustomer')
            ->where(' date(timestamp) between :start_date AND :end_date AND sponsor_id=:sponsor_id',[':sponsor_id'=>$sponsor_id,':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('full_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }

    public function actionGetuser($id){
        $query = new Query;

        $user_id = $id;

        $query->select('*')
            ->from('vwcustomer')
            ->where('id=:user_id',[':user_id'=>$user_id])
            ->orderBy('full_name');

        $command = $query->createCommand();
        $data = $command->queryOne();

        echo json_encode($data);

    }

    public function actionGetchildren($id){
        $query = new Query;

        $user_id = $id;

        $query->select('*')
            ->from('vwcustomer')
            ->where('sponsor_id=:user_id',[':user_id'=>$user_id])
            ->orderBy('id');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }

    public function actionGetreferrals(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];
        $sponsor_id = $_POST['sponsor_id'];

        $query->select('*')
            ->from('vw_payments')
            ->where(' date(created) between :start_date AND :end_date AND sponsor_id=:sponsor_id',[':sponsor_id'=>$sponsor_id,':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        $ret =[];

        foreach($data as $d){
            $amt = floatval($d['amount_due']);
            $ref_amt = $amt * (3/100);

            $d['ref_amt'] = $ref_amt;
            array_push($ret,$d);
        }

        echo json_encode($ret);

    }

    public function actionGetsubsidy(){
        $query = new Query;
        //print_r($_POST);die();
        $month = intval($_POST['month']) + 1;
        $year = $_POST['year'];
        $sponsor_id = $_POST['sponsor_id'];

        $query->select('*')
            ->from('vw_sales_invoices')
            ->where(' YEAR(created)= :year AND MONTH(created) = :month AND customer_id=:sponsor_id',[':sponsor_id'=>$sponsor_id,':month' => $month,':year'=>$year])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();
        echo json_encode($data);
    }

    public function actionDailycollection(){
        $query = new Query;
        $date_from = date('Y-m-d');
        //echo $date_from;die();
        $query->select('*')
            ->from('vw_payments')
            ->where(' date(created) = :start_date ',[':start_date' => $date_from])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        return $this->render('dailycollection',['data'=>$data]);

    }

    public function actionDeposits(){
        return $this->render('deposits');
    }

    public function actionGetdeposits(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];

       // print_r($_POST);die();

        $query->select('*')
            ->from('deposit')
            ->where(' date(created) between :start_date AND :end_date ',[':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('created');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);
    }

    public function actionSavedeposit(){
        $deposit = new Deposit();

        $collection_date = strtotime($_POST['collection_date']);
        $collection_date = date("Y/m/d H:i:s", $collection_date);

        $deposit_date = strtotime($_POST['deposit_date']);
        $deposit_date = date("Y/m/d H:i:s", $deposit_date);


        $deposit->type = $_POST['payment_type'];
        $deposit->account_number = $_POST['txtBankNumber'];
        $deposit->cheque_number = $_POST['txtChequeNumber'];
        $deposit->bank_name = $_POST['txtBankName'];
        $deposit->branch = $_POST['txtBranch'];
        $deposit->collection_date =$collection_date;
        $deposit->deposit_date =$deposit_date;
        $deposit->amount = $_POST['txtAmount'];

        if($deposit->save()){
            return $this->redirect(['/reports/deposits']);
        }else{
            print_r($deposit->getErrors());die();
        }
        

    }

    public function actionSalesbranch(){
        return $this->render('salesbranch');
    }

    public function actionSubsidy(){
        return $this->render('subsidy');
    }

    public function actionDepositsreport(){
        return $this->render('depositsreport');
    }

    public function actionGetdepositsummary(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];

        // print_r($_POST);die();

        $query->select('*')
            ->from('deposit')
            ->where(' date(created) between :start_date AND :end_date ',[':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('created');

        $command = $query->createCommand();
        $deposit = $command->queryAll();


        $query->select('*')
            ->from('vw_payments')
            ->where(' date(created) between :start_date AND :end_date ',[':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $collection = $command->queryAll();

        $data =['deposit'=>$deposit,'collection'=>$collection];

        echo json_encode($data);
    }

}
