<?php

namespace backend\controllers;
use Yii;

use common\models\OrderLines;
use common\models\OrderPayments;
use common\models\Orders;
use common\models\StockOnHand;
use common\models\OrdersSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\db\Query;


class InvoiceController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionNewinvoice(){
        $max = Orders::find()->max('id');
        return $this->render('newinvoice',['invoiceNum'=>$max]);
    }

    public function actionSaveinvoice(){
       //print_r($_POST);die();

        $order = new Orders();
        $transaction = yii::$app->db->beginTransaction();

        try{
            $order->customer_id = $_POST['hdCustomerId'];
            $order->sponsor_id = $_POST['hdSponsorId'];
            $order->grand_total = $_POST['hdGrandTotal'];
            $order->sub_total = $_POST['hdSubTotal'];
            $order->mini_subsidy = $_POST['hdMiniSubsidyDiscount'];
            $order->mini_subsidy_perc = $_POST['hdSubsidyPerc'];
            $order->discount = $_POST['hdTotalDiscount'];
            $order->amount_due = $_POST['hdAmountDue'];
            $order->mini_accumulated_sales=$_POST['hdAccumulatedSales'];
            $order->created_by = \Yii::$app->user->id;
            $order->is_invoice = 1;
            $order->is_paid=0;
            $order->terms=$_POST['hdTerms'];
            $order->prev_balance=$_POST['hdBalance'];
            $order->remaining_balance = $_POST['hdAmountDue'];
            $order->due_date = date('Y-m-d',strtotime("+". $_POST['hdTerms'] . " days" ));

            if($order->save()){
                $lineCount = $_POST['hdLineCount'];

                for($i=1;$i<=$lineCount;$i++) {
                    if(isset ($_POST['qty' . $i])) {
                        $line = new OrderLines();
                        $line->order_id = $order->id;
                        $line->qty = $_POST['qty' . $i];
                        $line->product_id =  $_POST['hd_pid' . $i];
                        $line->srp =  $_POST['hd_srp' . $i];
                        $line->total =  $_POST['hd_subtotal' . $i];
                        $line->discount =  $_POST['hd_discount' . $i];
                        $line->amount =  $_POST['hd_total' . $i];

                        if($line->save()){
                            $inventory = StockOnHand::findOne(['product_id'=>$line->product_id]);
                            $old = $inventory->quantity;
                            $new  = floatval($old) - floatval($line->qty);

                            $inventory->quantity = $new;
                            $inventory->save();
                        }
                    }

                }
                $transaction->commit();

                $newOrder =  Orders::findOne(['id'=>$order->id]);
                return $this->redirect(['/invoice/printreceipt', 'id' => $order->id]);
                //return $this->render('receipt',['order'=>$newOrder]);
            }

        }catch (Exception $ex){
            $transaction->rollBack();
        }

    }

    public function actionPrintreceipt($id){
        $newOrder =  Orders::findOne(['id'=>$id]);
        return $this->render('receipt',['order'=>$newOrder]);
    }

    public function actionInvoices(){
        return $this->render('invoices');
    }

    public function actionInvoicelist(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];

        $query->select('*')
            ->from('vw_sales_invoices')
            ->where(' date(created) between :start_date AND :end_date AND is_invoice=1',[':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy(['id'=>SORT_ASC]);

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }

    public function actionPayorder(){
       // print_r($_POST);die();
        $order = Orders::findOne(['id'=> $_POST['hdOrderId']]);
        $transaction = yii::$app->db->beginTransaction();

        try{
            $date_now = date("d F Y");
            $date_now = strtotime($date_now);
            $date_now = date("Y/m/d H:i:s", $date_now);

            $order->last_payment_date =$date_now;
            $balance = floatval($order->remaining_balance) - floatval($_POST['txtAmount']);
            $order->remaining_balance= $balance;
            if($balance <1 ){
                $order->is_paid=1;
                $order->remaining_balance= 0;
            }
            $order->save();

            $orderPayment = new OrderPayments();
            $orderPayment->order_id = $order->id;
            $orderPayment->total_amount = $_POST['hdGrandTotal'];
            $orderPayment->amount_paid = $_POST['txtAmount'];
            $orderPayment->payment_type = $_POST['payment_type'];
            $orderPayment->is_invoice = 1;
            if($_POST['payment_type'] == 'cheque') {
                $orderPayment->bank_name = $_POST['txtBankName'];
                $orderPayment->bank_number = $_POST['txtBankNumber'];
                $orderPayment->cheque_number = $_POST['txtChequeNumber'];
            }

            $orderPayment->save();
            $transaction->commit();

            //$newOrder =  Orders::findOne(['id'=>$order->id]);
            return $this->redirect(['/invoice/printreceipt', 'id' => $order->id]);

        }catch (Exception $ex){
            $transaction->rollBack();
        }
    }



}
