<?php

namespace backend\controllers;

use common\models\Orders;
use Yii;
use common\models\Customers;
use common\models\CustomersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;
use yii\db\Query;

/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customers models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Customers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Customers #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Customers model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Customers();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Customers",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post())){

                $image = UploadedFile::getInstance($model, 'image');

                if($image){
                    $ext = explode('.', $image->name);
                    $ext = end($ext);
                    $base_path = Yii::$app->basePath . '/' . 'web/uploads/';
                    $file_name = Yii::$app->security->generateRandomString().".{$ext}";
                    $path = $base_path . $file_name;

                    $model->photo_url = $file_name;
                    $image->saveAs($path);
                }

                if($model->save()){
                    //$image->saveAs($path);
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Create new Customers",
                        'content'=>'<span class="text-success">Create Customers success</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                    ];
                } else {
                    // error in saving model
                }

            }else{           
                return [
                    'title'=> "Create new Customers",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }


    /**
     * Creates a new Customers model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateemployee()
    {
        $request = Yii::$app->request;
        $model = new Customers();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Employee",
                    'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $image = UploadedFile::getInstance($model, 'image');

                if($image){
                    $ext = explode('.', $image->name);
                    $ext = end($ext);
                    $base_path = Yii::$app->basePath . '/' . 'web/uploads/';
                    $file_name = Yii::$app->security->generateRandomString().".{$ext}";
                    $path = $base_path . $file_name;

                    $model->photo_url = $file_name;
                    $image->saveAs($path);
                }
                $model->is_mini=2;

                if($model->save()){
                    //$image->saveAs($path);
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Create new Customers",
                        'content'=>'<span class="text-success">Create Customers success</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                    ];
                } else {
                    // error in saving model
                }

            }else{
                return [
                    'title'=> "Create new Customers",
                    'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Creates a new Customers model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreatemini()
    {
        $request = Yii::$app->request;
        $model = new Customers();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Customers",
                    'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post())){

                $image = UploadedFile::getInstance($model, 'image');

                if($image){
                    $ext = explode('.', $image->name);
                    $ext = end($ext);
                    $base_path = Yii::$app->basePath . '/' . 'web/uploads/';
                    $file_name = Yii::$app->security->generateRandomString().".{$ext}";
                    $path = $base_path . $file_name;

                    $model->photo_url = $file_name;
                    $image->saveAs($path);
                }

                $model->is_mini = 1;

                if($model->save()){
                    //$image->saveAs($path);
                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Create new Customers",
                        'content'=>'<span class="text-success">Create Customers success</span>',
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                    ];
                } else {
                    // error in saving model
                }

            }else{
                return [
                    'title'=> "Create new Customers",
                    'content'=>$this->renderAjax('create', [
                            'model' => $model,
                        ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['index', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Customers model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Customers #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post())){


                $image = UploadedFile::getInstance($model, 'image');

                if($image){
                    $ext = explode('.', $image->name);
                    $ext = end($ext);
                    $base_path = Yii::$app->basePath . '/' . 'web/uploads/';
                    $file_name = Yii::$app->security->generateRandomString().".{$ext}";
                    $path = $base_path . $file_name;

                    $model->photo_url = $file_name;
                    $image->saveAs($path);
                }

                if($model->save()){

                    return [
                        'forceReload'=>'#crud-datatable-pjax',
                        'title'=> "Customers #".$id,
                        'content'=>$this->renderAjax('view', [
                                'model' => $model,
                            ]),
                        'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                    ];
                } else {
                    // error in saving model
                }


            }else{
                 return [
                    'title'=> "Update Customers #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Customers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Customers model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Customers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionJsonlist($q = null){

        $query = new Query;

        $query->select('*')
            ->from('vwcustomer')
            ->where('full_name LIKE "%' . $q .'%"')
            ->orderBy('full_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        $ret =[];
        foreach($data as $d){
            $result = \Yii::$app->db->createCommand("select get_customer_purchase_current_month(:paramName1)")
                ->bindValue(':paramName1' , $d['id'] );

            $sales = floatVal($result->queryScalar());
            $d['purchase']=$sales;
            $subsidy=0;

            if($sales<5000){
                $d['purchase_percentage'] = 0;
                $d['subsidy_amt'] = 0;
            }elseif($sales>5000 && $sales<10000){
                $d['purchase_percentage'] = 5;
                $subsidy = $sales * 5;
                $d['subsidy_amt'] = $subsidy;
            }elseif($sales>9999 && $sales<15000){
                $d['purchase_percentage'] = 10;
                $subsidy = $sales * 10;
                $d['subsidy_amt'] = $subsidy;
            }elseif($sales>14999 && $sales<19999){
                $d['purchase_percentage'] = 15;
                $subsidy = $sales * 15;
                $d['subsidy_amt'] = $subsidy;
            }elseif($sales>19999 && $sales<24999){
                $d['purchase_percentage'] = 20;
                $subsidy = $sales * 20;
                $d['subsidy_amt'] = $subsidy;
            }
            elseif($sales >=25000 ){
                $d['purchase_percentage'] = 25;
                $subsidy = $sales * 25;
                $d['subsidy_amt'] = $subsidy;
            }

            if($d['credit_limit']<25000){
                $d['terms'] ='45';
            }else{
                $d['terms'] = '60';
            }

            $d['subsidy_amt'] =0;
            $d['sales'] = $sales;
            $d['eligible'] =true;

            $r = \Yii::$app->db->createCommand("select get_customer_invoice(:paramName1,:paramName2)")
                ->bindValue(':paramName1' , $d['id'] )
                ->bindValue(':paramName2' , 0 );
            $balance = floatVal($r->queryScalar());

            //$orders = Orders::findAll(['customer_id'=>$d['id'],'is_invoice'=>'1','is_paid'=>'0']);
            if($d['credit_limit'] < $balance)  $d['eligible']=false;
            $d['balance'] = $balance;

            array_push($ret,$d);
        }
        //print_r($ret);die();
        echo json_encode($ret);
    }

    public function actionMcolist($q = null){

        $query = new Query;

        $query->select('*')
            ->from('vwcustomer')
            ->where('full_name LIKE "%' . $q .'%" AND is_mini=1')
            ->orderBy('full_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        $ret =[];
        foreach($data as $d){
            $result = \Yii::$app->db->createCommand("select get_customer_purchase_current_month(:paramName1)")
                ->bindValue(':paramName1' , $d['id'] );

            $sales = floatVal($result->queryScalar());
            $d['purchase']=$sales;
            $subsidy=0;

            if($sales<5000){
                $d['purchase_percentage'] = 0;
                $d['subsidy_amt'] = 0;
            }elseif($sales>5000 && $sales<10000){
                $d['purchase_percentage'] = 5;
                $subsidy = $sales * 5;
                $d['subsidy_amt'] = $subsidy;
            }elseif($sales>9999 && $sales<15000){
                $d['purchase_percentage'] = 10;
                $subsidy = $sales * 10;
                $d['subsidy_amt'] = $subsidy;
            }elseif($sales>14999 && $sales<19999){
                $d['purchase_percentage'] = 15;
                $subsidy = $sales * 15;
                $d['subsidy_amt'] = $subsidy;
            }elseif($sales>19999 && $sales<24999){
                $d['purchase_percentage'] = 20;
                $subsidy = $sales * 20;
                $d['subsidy_amt'] = $subsidy;
            }
            elseif($sales >=25000 ){
                $d['purchase_percentage'] = 25;
                $subsidy = $sales * 25;
                $d['subsidy_amt'] = $subsidy;
            }

            if($d['credit_limit']<25000){
                $d['terms'] ='45';
            }else{
                $d['terms'] = '60';
            }

            $d['sales'] = $sales;
            $d['eligible'] =true;

            $r = \Yii::$app->db->createCommand("select get_customer_invoice(:paramName1,:paramName2)")
                ->bindValue(':paramName1' , $d['id'] )
                ->bindValue(':paramName2' , 0 );
            $balance = floatVal($r->queryScalar());

            //$orders = Orders::findAll(['customer_id'=>$d['id'],'is_invoice'=>'1','is_paid'=>'0']);
            if($d['credit_limit'] < $balance)  $d['eligible']=false;
            $d['balance'] = $balance;

            array_push($ret,$d);
        }
        //print_r($ret);die();
        echo json_encode($ret);
    }
}
