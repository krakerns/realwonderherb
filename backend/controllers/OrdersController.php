<?php

namespace backend\controllers;

use Yii;

use common\models\OrderLines;
use common\models\OrderPayments;
use common\models\Orders;
use common\models\StockOnHand;
use common\models\OrdersSearch;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\db\Query;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "Orders #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Orders model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Orders();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Orders",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Orders",
                    'content'=>'<span class="text-success">Create Orders success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new Orders",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Orders #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Orders #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Orders #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Orders model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionPos(){
        return $this->render('pos');
    }

    public function actionSaveorders(){
        //print_r($_POST);die();

        $order = new Orders();
        $transaction = yii::$app->db->beginTransaction();

        try{
            $order->customer_id = $_POST['hdCustomerId'];
            $order->sponsor_id = $_POST['hdSponsorId'];
            $order->grand_total = $_POST['hdGrandTotal'];
            $order->sub_total = $_POST['hdSubTotal'];
            $order->mini_subsidy = $_POST['hdMiniSubsidyDiscount'];
            $order->mini_subsidy_perc = $_POST['hdSubsidyPerc'];
            $order->discount = $_POST['hdTotalDiscount'];
            $order->amount_due = $_POST['hdAmountDue'];
            $order->mini_accumulated_sales=$_POST['hdAccumulatedSales'];
            $order->created_by = \Yii::$app->user->id;

            if($order->save()){
                $lineCount = $_POST['hdLineCount'];

                for($i=1;$i<=$lineCount;$i++) {
                    if(isset ($_POST['qty' . $i])) {
                        $line = new OrderLines();
                        $line->order_id = $order->id;
                        $line->qty = $_POST['qty' . $i];
                        $line->product_id =  $_POST['hd_pid' . $i];
                        $line->srp =  $_POST['hd_srp' . $i];
                        $line->total =  $_POST['hd_subtotal' . $i];
                        $line->discount =  $_POST['hd_discount' . $i];
                        $line->amount =  $_POST['hd_total' . $i];

                        if($line->save()){
                            $inventory = StockOnHand::findOne(['product_id'=>$line->product_id]);
                            $old = $inventory->quantity;
                            $new  = floatval($old) - floatval($line->qty);

                            $inventory->quantity = $new;
                            $inventory->save();
                        }
                    }

                }

                $orderPayment = new OrderPayments();
                $orderPayment->order_id = $order->id;
                $orderPayment->total_amount = $_POST['hdGrandTotal'];
                $orderPayment->amount_paid = $_POST['txtAmount'];
                $orderPayment->change = $_POST['txtChange'];
                $orderPayment->payment_type = $_POST['payment_type'];
                if($_POST['payment_type'] == 'cheque') {
                    $orderPayment->bank_name = $_POST['txtBankName'];
                    $orderPayment->bank_number = $_POST['txtBankNumber'];
                    $orderPayment->cheque_number = $_POST['txtChequeNumber'];
                }

                $orderPayment->save();
                $transaction->commit();

                $newOrder =  Orders::findOne(['id'=>$order->id]);
                return $this->redirect(['/orders/printreceipt', 'id' => $order->id]);
                //return $this->render('receipt',['order'=>$newOrder]);
            }

        }catch (Exception $ex){
            $transaction->rollBack();
        }

    }

    public function actionPrintreceipt($id){
        $newOrder =  Orders::findOne(['id'=>$id]);
        return $this->render('receipt',['order'=>$newOrder]);
    }

    public function actionInvoices(){
        return $this->render('salesinvoices');
    }

    public function actionOrderslist(){
        $query = new Query;

        $date_from = $_POST['datefrom'];
        $date_to = $_POST['dateto'];

        $query->select('*')
            ->from('vw_sales_invoices')
            ->where(' date(created) between :start_date AND :end_date AND is_invoice=0',[':start_date' => $date_from,':end_date'=>$date_to])
            ->orderBy('customer_name');

        $command = $query->createCommand();
        $data = $command->queryAll();

        echo json_encode($data);

    }
}
