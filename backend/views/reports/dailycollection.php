<?php


$this->registerJsFile('@web/js/jquery.dataTables.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/dataTables.bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

    <div class="row">

        <div class="col-md 12">
            <h2>Daily Collection</h2>

        </div>
        <hr />
    </div>
    <button style="display:none" id="btnGenerate"></button>
    <div class="row">
        <div class="col-md-12">

            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Sales</th>
                    <th style="text-align: right">Payment Received</th>
                </tr>
                </thead>

                <tbody>
                    <?php
                        $cash = 0;
                        $cheque = 0;
                        $card=0;
                        $amount_paid = 0;
                        foreach($data as $d){
                            echo '<td>'.$d['created'].'</td>';
                            echo '<td>'.$d['customer_name'].'</td>';
                            echo '<td>'.$d['payment_type'].'</td>';
                            echo '<td style="text-align: right">'.$d['amount_paid'].'</td>';
                            $amount_paid+=$d['amount_paid'];
                            switch($d['payment_type']){
                                case 'cash':
                                    $cash += $d['amount_paid'];
                                    break;
                                case 'card':
                                    $card += $d['amount_paid'];
                                    break;
                                case 'cheque':
                                    $cheque += $d['amount_paid'];
                                    break;
                            }
                        }
                    ?>
                </tbody>

                <tfoot>
                <tr style="border-top:1px solid black !important;margin-top:10px;font-size:14px !important;">
                    <td colspan="3" style="text-align: right">
                        <span style="font-weight:bold;">Total</span>
                        <input type="hidden" name="hdSubTotal" id="hdSubTotal" value="0" />
                    </td>
                    <td style="text-align: right;font-weight:bold;">
                        <span id="main_total"><?= number_format($amount_paid,2,".",",");?></span>
                    </td>
                </tr>
                </tfoot>
            </table>

        </div>
    </div>
<br /><br /><br /><br />
    <div class="row">
        <div class="col-md-9">
                &nbsp;
        </div>

        <div class="col-md-3">
            <p style="font-weight: bold;font-size: 14px;text-align: right"> Cheque Payment : <?= number_format($cheque,2,".",","); ?></p>
            <p style="font-weight: bold;font-size: 14px;text-align: right"> Cash Payment : <?= number_format($cash,2,".",","); ?></p>
            <p style="font-weight: bold;font-size: 14px;text-align: right"> Card Payment : <?= number_format($card,2,".",","); ?></p>
        </div>
   </div>
<?php
$scrpt = <<<EOD
var tbl_history;
$(document).ready(function () {
    $('#example').DataTable();


});

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>