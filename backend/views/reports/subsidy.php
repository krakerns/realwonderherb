<?php
use yii\helpers\Html;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;

$this->registerJsFile('@web/js/jquery.dataTables.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/dataTables.bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

    <div class="row">

        <div class="col-md 12">
            <h2>Subsidy Report</h2>

        </div>
        <hr />
    </div>

    <div class="row">
        <h4>Filter</h4>

        <div class="row">

            <div class="col-md-4" id="get_commission_type">
                <p><b>Dealer:</b></p>
                <?=
                Typeahead::widget([
                    'name' => 'country',
                    'options' => ['placeholder' => 'Select Dealer'],
                    'pluginOptions' => ['highlight'=>true],
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'full_name',
                            'remote' => [
                                'url' => Url::to(['/customers/jsonlist']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginEvents'=>[
                        "typeahead:selected" => "function(obj, item) {
                            console.log(item);
                                $('#hdId').val(item.id);
                                $('#idName').html(item.full_name);
                                $('#is_mini').val(item.is_mini);
                            }",
                    ]
                ]);
                ?>

                <input type="hidden" id="hdId" name="hdId" value="0" />
                <input type="hidden" id="is_mini" name="is_mini" value="0" />
            </div>

            <div class="col-md-3" id="get_commission_type">
                <p><b>Month:</b></p>
                <?=
                Html::dropDownList('month','',['January','February','March','April','May','June','July','August','September','October','November','December'],['id'=>'month','class'=>'form-control']);
                ?>
            </div>


            <div class="col-md-3" id="get_commission_period">
                <div id="get_commission_period_options">
                    <p><b>Year:</b><p>
                </div>
                <div id="select_commission_period" >
                    <?=
                    Html::dropDownList('year','',['2017'=>'2017','2018'=>'2018','2019'=>'2019','2020'=>'2020','2021'=>'2021','2022'=>'2022','2023'=>'2023','2024'=>'2024','2025'=>'2025','2026'=>'2026','2027'=>'2027','2028'=>'2028'],['id'=>'year','class'=>'form-control']);
                    ?>
                </div>
            </div>

            <div class="col-md-2" id="get_commissions" >
                <a id="btn_generate" style="margin-top: 30px"  href="javascript:void(0);" class="btn btn-success"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>&nbsp; View Report</a>

            </div>


        </div>

        <hr />
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="row" style="text-align: right;">
                <input type="button" class="btn btn-success" value="Print" id="prnt">
            </div>

            <div id="dvContent">

                <h4 id="idName"></h4>
                <br /><br />
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Date</th>
                        <th>OR #</th>
                        <th>With Subsidy</th>
                        <th>With Subsidy</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
                <br /><br /><br />
                <div style="display: table;margin: auto;font-size:19px;">
                    <span>Total : </span> &nbsp;<span id="spnTotal"></span><br /><br />

                    <span>Subsidy % : </span> &nbsp;<span id="spnPerc"></span><br /><br />

                    <span>Subsidy Amount: </span> &nbsp;<span id="spnSubsidy"></span><br /><br />
                </div>

            </div>


        </div>
    </div>

<?php
$scrpt = <<<EOD
var tbl_history;
$(document).ready(function () {


    NewOrders.init();

    $('#btn_generate').on('click',function(){


		var month = $('#month').val();
		var year = $('#year').val();
		var sponsor_id = $('#hdId').val();
		var data = {month:month,year:year,sponsor_id:sponsor_id};


		$.ajax({
		  url: '/reports/getsubsidy',
		  type:"POST",
		  data: data,
		  dataType:'json',
		  success: function(data){
            loadTable(data);
		  }
		});
	});


	$('#prnt').click(function(){
        var divContents = $("#dvContent").html();
        var printWindow = window.open('', '', 'height=600,width=800');
        printWindow.document.write('<html><head><title></title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();

    });


});

function loadTable(data) {
    $('#example tbody').html('');
    var html = "";
    var total = 0;
    $.each(data, function (index, object) {
        var rate = 0;

        total += parseFloat(object.amount_due);

        html += "<tr>";
        html += "<td>"+object.created+"</td>";
        html += "<td>"+pad(object.id,9)+"</td>";
        html += "<td>"+object.amount_due+"</td>";
        html += "<td>0.00</td>";
        html += "</tr>";
    });

    $('#example tbody').append(html);
    $('#spnTotal').html(total.toFixed(2));
    getSubisidyPerc(parseFloat(total));
}

function getSubisidyPerc(total){
   var perc =0;
   var subsidy =0;

   if(total<5000){
        perc = 0;
    }else if(total>5000 && total<10000){
        perc = 5;
        subsidy = parseFloat(total) * .05;
    }else if(total>9999 && total<15000){
        perc = 10;
        subsidy = parseFloat(total) * .10;
    }else if(total>14999 && total<19999){
        perc = 15;
        subsidy = parseFloat(total) * .15;
    }else if(total>19999 && total<24999){
        perc = 20;
        subsidy = parseFloat(total) * .20;
    }
    else if(total >=25000 ){
        perc = 25;
        subsidy = parseFloat(total) * .25;
    }

    if($('#is_mini').val()!='2'){
        $('#spnPerc').html(perc + ' %');
        $('#spnSubsidy').html(subsidy.toFixed(2));
    }else{
        $('#spnPerc').html( '25 %');
        subsidy = parseFloat(total) * .25;
        $('#spnSubsidy').html(subsidy.toFixed(2));
    }


}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>