<?php
use yii\jui\DatePicker;
use yii\helpers\Html;

$this->registerJsFile('@web/js/jquery.dataTables.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/dataTables.bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

    <div class="row">

        <div class="col-md 12">
            <h2>Collection/Deposit Summary</h2>

        </div>
        <hr />
    </div>

    <div class="row">
        <h4>Filter</h4>

        <div class="row">
            <div class="col-md-3" id="get_commission_type">
                <p><b>From:</b></p>
                <?=
                DatePicker::widget(['name' => 'from','options' => ['class' => 'form-control','id'=>'from'],'dateFormat' => 'yyyy-MM-dd']);
                ?>
            </div>


            <div class="col-md-3" id="get_commission_period">
                <div id="get_commission_period_options">
                    <p><b>To:</b><p>
                </div>
                <div id="select_commission_period" >
                    <?=
                    DatePicker::widget(['name' => 'to','options' => ['class' => 'form-control','id'=>'to'],'dateFormat' => 'yyyy-MM-dd']);
                    ?>
                </div>
            </div>

            <div class="col-md-3" id="get_commissions" >
                <a id="btn_generate" style="margin-top: 30px"  href="javascript:void(0);" class="btn btn-success"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>&nbsp; View Report</a>

            </div>
        </div>

        <hr />
    </div>

    <div class="row">
        <div class="col-md-12">

            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Collection(Cash)</th>
                    <th>Collection(Cheque)</th>
                    <th>Collection(Total)</th>
                    <th>Deposit(Cash)</th>
                    <th>Deposit(Cheque)</th>
                    <th>Deposit(Total)</th>
                    <th>Remarks</th>
                </tr>
                </thead>

                <tbody>

                </tbody>
            </table>

        </div>
    </div>

<?php
$scrpt = <<<EOD
var tbl_history;
$(document).ready(function () {
    $('#example').DataTable();

    NewOrders.init();

    $('#btn_generate').on('click',function(){


		var date_from = $('#from').val();
		var date_to = $('#to').val();		
		var data = {datefrom:date_from,dateto:date_to};


		$.ajax({
		  url: '/reports/getdepositsummary',
		  type:"POST",
		  data: data,
		  dataType:'json',
		  success: function(data){
            loadTable(data);
		  }
		});
	});

    tbl_history = $("#example").DataTable();
});

function loadTable(data) {

    tbl_history.destroy();
    $('#example tbody').html('');

    $('#example tbody').html('');
    var html = "";
    var total = 0;
    
    var startDate =new Date($('#from').val());
    var endDate = new Date($('#to').val());
    var counter = 1;
    while(startDate <= endDate){
        var ssDate = startDate.toDateString()
        var collections = _.filter(data.collection,(d)=>{
            return ssDate == new Date(d.created).toDateString();               
        })
        
        var deposits = _.filter(data.deposit,(d)=>{
            return ssDate == new Date(d.created).toDateString();               
        })
        var collection_cash = 0;
        var collection_card = 0;
        
        var deposit_cash = 0;
        var deposit_card = 0;
        
        _.map(collections,(c)=>{
            if(c.payment_type == 'cash' || c.payment_type == 'card'){
                collection_cash += parseFloat(c.total_amount);
            }else{
                collection_card += parseFloat(c.total_amount)
            }
        });
        
        _.map(deposits,(c)=>{
            if(c.type == 'cash' || c.type == 'card'){
                deposit_cash += parseFloat(c.amount);
            }else{
                deposit_card += parseFloat(c.amount)
            }
        });
        
        var datestring = startDate.getDate()  + "-" + (startDate.getMonth()+1) + "-" + startDate.getFullYear()
        html += "<tr>";
        html += "<td data-order='"+counter+"'>"+datestring+"</td>";
        html += "<td>"+collection_cash.toFixed(2)+"</td>";
        html += "<td>"+collection_card.toFixed(2)+"</td>";
        html += "<td>"+(collection_cash + collection_card).toFixed(2) +"</td>";
        html += "<td>"+deposit_cash.toFixed(2)+"</td>";
        html += "<td>"+deposit_card.toFixed(2)+"</td>";
        html += "<td>"+(deposit_cash + deposit_card).toFixed(2) +"</td>";
        html += "<td></td>";
        html += "</tr>";
        
        startDate = new Date(startDate.setDate(startDate.getDate() + 1));
        counter++;
    }
    
    <!--$.each(data, function (index, object) {-->

        <!--html += "<tr>";-->
        <!--html += "<td><a href='/orders/printreceipt?id="+object.id+"'>"+pad(object.id,9)+"</a></td>";-->
        <!--html += "<td>"+object.customer_name+"</td>";-->
        <!--html += "<td>"+object.sponsor_name+"</td>";-->
         <!--html += "<td>"+object.payment_type+"</td>";-->
        <!--html += "<td>"+object.created+"</td>";-->
        <!--html += "<td>"+object.grand_total+"</td>";-->
        <!--html += "<td>"+object.mini_subsidy+"</td>";-->
        <!--html += "<td>"+object.amount_due+"</td>";-->
        <!--html += "</tr>";-->

    <!--});-->

    $('#example tbody').append(html);
    tbl_history = $('#example').DataTable({pageLength:25});
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>