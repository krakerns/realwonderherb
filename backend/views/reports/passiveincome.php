<?php
use yii\jui\DatePicker;
use yii\helpers\Html;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;

$this->registerJsFile('@web/js/jquery.dataTables.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/dataTables.bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

    <div class="row">

        <div class="col-md 12">
            <h2>Referral Report</h2>

        </div>
        <hr />
    </div>

    <div class="row">
        <h4>Filter</h4>

        <div class="row">

            <div class="col-md-4" id="get_commission_type">
                <p><b>Dealer:</b></p>
                <?=
                Typeahead::widget([
                    'name' => 'country',
                    'options' => ['placeholder' => 'Select Referrer'],
                    'pluginOptions' => ['highlight'=>true],
                    'dataset' => [
                        [
                            'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                            'display' => 'full_name',
                            'remote' => [
                                'url' => Url::to(['/customers/jsonlist']) . '?q=%QUERY',
                                'wildcard' => '%QUERY'
                            ]
                        ]
                    ],
                    'pluginEvents'=>[
                        "typeahead:selected" => "function(obj, item) {
                            console.log(item);
                                $('#hdId').val(item.id);
                                $('#idName').html(item.full_name);

                                if(item.is_mini=='1'){
                                    $('#idMaintenance').html('5000');
                                }else if(item.is_mini=='0'){
                                    $('#idMaintenance').html('700');
                                }else if(item.is_mini=='2'){
                                    $('#idMaintenance').html('700');
                                }
                            }",
                    ]
                ]);
                ?>

                <input type="hidden" id="hdId" name="hdId" value="0" />
            </div>

            <div class="col-md-3" id="get_commission_type">
                <p><b>From:</b></p>
                <?=
                DatePicker::widget(['name' => 'from','options' => ['class' => 'form-control','id'=>'from'],'dateFormat' => 'yyyy-MM-dd']);
                ?>
            </div>


            <div class="col-md-3" id="get_commission_period">
                <div id="get_commission_period_options">
                    <p><b>To:</b><p>
                </div>
                <div id="select_commission_period" >
                    <?=
                    DatePicker::widget(['name' => 'to','options' => ['class' => 'form-control','id'=>'to'],'dateFormat' => 'yyyy-MM-dd']);
                    ?>
                </div>
            </div>

            <div class="col-md-2" id="get_commissions" >
                <a id="btn_generate" style="margin-top: 30px"  href="javascript:void(0);" class="btn btn-success"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>&nbsp; View Report</a>

            </div>


        </div>

        <hr />
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="row" style="text-align: right;">
                <input type="button" class="btn btn-success" value="Print" id="prnt">
            </div>

            <div id="dvContent">

                <h4 id="idName"></h4>
                <br /><br />
                <table id="example" class="display" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>Name of Member</th>
                        <th>With RF</th>
                        <th>Without RF</th>
                        <th>Rate %</th>
                        <th>Amount</th>
                    </tr>
                    </thead>

                    <tbody>

                    </tbody>
                </table>
                <br /><br /><br />
                <div style="display: table;margin: auto;font-size:18px;">
                    <span>Maintenance : </span> &nbsp;<span id="idMaintenance"></span><br /><br />
                    <span>For Release : </span> &nbsp;<span id="spnTotal"></span>
                </div>

            </div>


        </div>
    </div>

<?php
$scrpt = <<<EOD
var tbl_history;
$(document).ready(function () {


    NewOrders.init();

    $('#btn_generate').on('click',function(){


		var date_from = $('#from').val();
		var date_to = $('#to').val();
		var sponsor_id = $('#hdId').val();
		var data = {datefrom:date_from,dateto:date_to,sponsor_id:sponsor_id};


		$.ajax({
		  url: '/reports/getreferrals',
		  type:"POST",
		  data: data,
		  dataType:'json',
		  success: function(data){
            loadTable(data);
		  }
		});
	});


	$('#prnt').click(function(){
        var divContents = $("#dvContent").html();
        var printWindow = window.open('', '', 'height=600,width=800');
        printWindow.document.write('<html><head><title></title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();

    });


});

function loadTable(data) {




    $('#example tbody').html('');
    var html = "";
    var total = 0;
    $.each(data, function (index, object) {
        var rate = 0;

        if(parseFloat(object.amount_due)>699 && parseFloat(object.amount_due)<1000){
            rate = .03;

        }else if(parseFloat(object.amount_due)>999 && parseFloat(object.amount_due)<1500){
            rate= .04
        }else if(parseFloat(object.amount_due)>1499){
            rate= .05
        }

        var val = parseFloat(object.amount_due) * rate;
        total += val;
        html += "<tr>";
        html += "<td>"+object.customer_name+"</td>";
        html += "<td>"+object.amount_due+"</td>";
        html += "<td>0.00</td>";
        html += "<td>"+rate+"%</td>";
        html += "<td>"+val+"</td>";
        html += "</tr>";
    });

    $('#example tbody').append(html);
    $('#spnTotal').html(total.toFixed(2));

}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>