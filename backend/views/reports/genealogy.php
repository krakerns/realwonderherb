<?php
/**
 * Created by PhpStorm.
 * User: jvjunsay
 * Date: 2/20/17
 * Time: 10:06 AM
 */
$this->registerJsFile('@web/js/jquery.treetable.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<div class="row">
    <div class="col-md-12">
        <h3>Genealogy</h3>
        <hr />
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <select id="sel_toggle" class="form-control">
            <option value="1">Collapse</option>
            <option value="0">Expand</option>

        </select>
    </div>
</div>
<div style="clear: both;"><br /></div>
<div class="row">
    <div class="col-md-12">
        <table id="enroller" class="table table-striped table-bordered" >
            <thead>
            <tr>
                <th>Customer #</th>
                <th>Name</th>
                <th>Email</th>
                <th>Active</th>
                <th>Referrer</th>
            </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>

<?php
$scrpt = <<<EOD
var tbl_history;
$(document).ready(function () {
    setUpTree();

    $("#sel_toggle").change(function(){
        var select = $(this).val();
        if(select == "1")
                $("#enroller").treetable('collapseAll');
        else
        $("#enroller").treetable('expandAll');
    });
});

function setUpTree(){
    var user_id = '101';
    var url = '/reports/getuser?id=' + user_id;

    $.ajax({
            url: url,
            type:"GET",
            dataType: "json",
            success:function(object){
                var html="";
                var level = 0;

                html += '<tr  data-tt-id="'+object.id+'" data-tt-branch="true">';
                html += "<td>"+pad(object.id,9)+"</td>";
                html += "<td>"+object.full_name+"</td>";
                html += "<td>"+object.email+"</td>";
                html += "<td>"+object.active+"</td>";
                html += "<td>"+object.sponsor_name+"</td>";
                html += "</tr>";

                $('#enroller tbody').append(html);

                $("#enroller").treetable({
                    expandable: true,
                    onNodeCollapse: function() {
                        var node = this;
                            $("#enroller").treetable("collapseNode",node.id);
                    },
                    onNodeExpand: function() {
                        var node = this;

                        if(node.children.length > 0){
                            $("#enroller").treetable("expandNode",node.id);
                        }else{
                            var mUser = node.id;
                            var url2 ='/reports/getchildren?id=' + mUser;

                            $.ajax({
                                url: url2,
                                type:"GET",
                                dataType: "json",
                                success:function(data){
                                    var m_html = '';

                                            $.each(data, function(index,obj){

                                                m_html += '<tr  data-tt-id="'+obj.id+'" data-tt-parent-id="'+node.id+'" data-tt-branch="true">';
                                                m_html += "<td>"+pad(obj.id,9)+"</td>";
                                                m_html += "<td>"+obj.full_name+"</td>";
                                                m_html += "<td>"+obj.email+"</td>";
                                                m_html += "<td>"+obj.active+"</td>";

                                                m_html += "<td>"+obj.sponsor_name+"</td>";
                                                m_html += "</tr>";
                                            });

                                             $("#enroller").treetable("loadBranch", node, m_html);
                                }
                            });
                        }
                    }
                });
            }
        });

}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>