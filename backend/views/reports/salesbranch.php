<?php
use yii\jui\DatePicker;
use yii\helpers\Html;
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use yii\helpers\Url;

$this->registerJsFile('@web/js/jquery.dataTables.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/dataTables.bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

    <div class="row">

        <div class="col-md 12">
            <h2>Sales Report - Branch</h2>

        </div>
        <hr />
    </div>

    <div class="row">
        <h4>Filter</h4>

        <div class="row">


            <div class="col-md-3" id="get_commission_type">
                <p><b>From:</b></p>
                <?=
                DatePicker::widget(['name' => 'from','options' => ['class' => 'form-control','id'=>'from'],'dateFormat' => 'yyyy-MM-dd']);
                ?>
            </div>


            <div class="col-md-3" id="get_commission_period">
                <div id="get_commission_period_options">
                    <p><b>To:</b><p>
                </div>
                <div id="select_commission_period" >
                    <?=
                    DatePicker::widget(['name' => 'to','options' => ['class' => 'form-control','id'=>'to'],'dateFormat' => 'yyyy-MM-dd']);
                    ?>
                </div>
            </div>

            <div class="col-md-2" id="get_commissions" >
                <a id="btn_generate" style="margin-top: 30px"  href="javascript:void(0);" class="btn btn-success"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>&nbsp; View Report</a>

            </div>

        </div>

        <hr />
    </div>

    <div class="row">
        <div class="col-md-12">

            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th>Date</th>
                    <th>Customer</th>
                    <th>Regular Amount</th>
                    <th>Promo Amount</th>
                    <th>Total Gross</th>
                    <th>Total Net</th>
                </tr>
                </thead>

                <tbody>

                </tbody>
            </table>

        </div>
    </div>

<?php
$scrpt = <<<EOD
var tbl_history;
$(document).ready(function () {
    $('#example').DataTable();

    NewOrders.init();

    $('#btn_generate').on('click',function(){


		var date_from = $('#from').val();
		var date_to = $('#to').val();
		var sponsor_id = $('#hdSponsorId').val();
		var data = {datefrom:date_from,dateto:date_to,sponsor_id:sponsor_id};


		$.ajax({
		  url: '/reports/getbranch',
		  type:"POST",
		  data: data,
		  dataType:'json',
		  success: function(data){
            loadTable(data);
		  }
		});
	});

    tbl_history = $("#example").DataTable();
});

function loadTable(data) {

    tbl_history.destroy();
    $('#example tbody').html('');

    $('#example tbody').html('');
    var html = "";
    var total = 0;

    $.each(data, function (index, object) {
        var net = parseFloat(object.amount_due) - (parseFloat(object.amount_due) * .3)
        html += "<tr>";
        html += "<td>"+object.created+"</td>";
        html += "<td>"+object.customer_name+"</td>";
        html += "<td>"+object.amount_due+"</td>";
        html += "<td>0.00</td>";
        html += "<td>"+object.amount_due+"</td>";
        html += "<td>"+net+"</td>";

        html += "</tr>";

        total += parseFloat(object.amount_due);

    });

    $('#example tbody').append(html);
    $('#spnTotal').html(total.toFixed(2));
    tbl_history = $('#example').DataTable({pageLength:25});
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>