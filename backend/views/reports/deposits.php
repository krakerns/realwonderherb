<?php
use yii\jui\DatePicker;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$this->registerJsFile('@web/js/jquery.dataTables.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/dataTables.bootstrap.min.js',['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);

?>

<div class="row">

    <div class="col-md 12">
        <h2>Deposits</h2>
    </div>
    <hr />
</div>



<div class="row">
    <h4>Filter</h4>

    <div class="row">
        
        <div class="col-md-3" id="get_commission_type">
            <p><b>From:</b></p>
            <?=
            DatePicker::widget(['name' => 'from','value'=>date("Y-m-d", strtotime("-1 months")),'options' => ['class' => 'form-control','id'=>'from'],'dateFormat' => 'yyyy-MM-dd']);
            ?>
        </div>


        <div class="col-md-3" id="get_commission_period">
            <div id="get_commission_period_options">
                <p><b>To:</b><p>
            </div>
            <div id="select_commission_period" >
                <?=
                DatePicker::widget(['name' => 'to','value'=>date('Y-m-d'),'options' => ['class' => 'form-control','id'=>'to'],'dateFormat' => 'yyyy-MM-dd']);
                ?>
            </div>
        </div>

        <div class="col-md-3" id="get_commissions" >
            <a id="btn_generate" style="margin-top: 30px"  href="javascript:void(0);" class="btn btn-success"><span class='glyphicon glyphicon-search' aria-hidden='true'></span>&nbsp; View Report</a>

        </div>
    </div>

    <hr />
</div>

<div class="row">
    <div class="col-md-12">

        <table id="example" class="display" cellspacing="0" width="100%">
            <thead>
            <tr>
                <th>Bank Name</th>
                <th>Branch</th>                
                <th>Payment Type</th>
                <th>Account No.</th>
                <th>Cheque No.</th>
                <th>Collection Date</th>
                <th>Deposit Date</th>
                <th>Amount</th>
            </tr>
            </thead>

            <tbody>

            </tbody>
        </table>

    </div>
</div>

<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <?php $form = ActiveForm::begin([
        'id'=>'add-item-form',
        'action' => '/reports/savedeposit',
        'options'=>['enctype'=>'multipart/form-data','class'=>'add-item-form','name'=>'add-item-form'],
    ]); ?>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h2 class="modal-title">Add Deposit</h2>
            </div>
            
            <div class="modal-body">
                 <div class="products-form">
                     <div class="row">
                             <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">PAYMENT TYPE:</label>
                                    <?= Html::radioList('payment_type',['cash'], ArrayHelper::map([['id'=>'cash','name'=>'Cash'],
                                        ['id'=>'card','name'=>'Card'],
                                        ['id'=>'cheque','name'=>'Cheque']
                                    ], 'id', 'name'),['id'=>'payment_type']) ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="control-label">BANK NAME:</label>
                                    <input type="text" class="form-control" id="txtBankName" name="txtBankName" value=""   />
                                </div>
                            </div>

                            <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="control-label">BRANCH:</label>
                                    <input type="text" class="form-control" id="txtBranch" name="txtBranch" value=""   />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">ACCOUNT NUMBER:</label>
                                    <input type="text" class="form-control" id="txtBankNumber" name="txtBankNumber" value=""   />
                                </div>
                            </div>

                            <div class="col-md-12" id="divCheque" style="display:none;">
                                <div class="form-group">
                                    <label class="control-label">CHEQUE NUMBER:</label>
                                    <input type="text" class="form-control" id="txtChequeNumber" name="txtChequeNumber" value=""   />
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">COLLECTION DATE:</label>
                                    <?=
                                        DatePicker::widget(['name' => 'collection_date','value'=>date('Y-m-d'),'options' => ['class' => 'form-control','id'=>'collection_date'],'dateFormat' => 'yyyy-MM-dd']);
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">DEPOSIT DATE:</label>
                                    <?=
                                        DatePicker::widget(['name' => 'deposit_date','value'=>date('Y-m-d'),'options' => ['class' => 'form-control','id'=>'deposit_date'],'dateFormat' => 'yyyy-MM-dd']);
                                    ?>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">AMOUNT:</label>
                                    <input type="text" class="form-control" id="txtAmount" name="txtAmount" value="0.00"   />
                                </div>
                            </div>
                            
                        </div>
                 </div>            
            </div>
            
            <div class="modal-footer">
                <input type="submit" id="btnProceed" class="btn btn-primary" value="Proceed"/>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
            
        </div>
    </div>   
    <?php ActiveForm::end(); ?>       
</div>

<?php
$scrpt = <<<EOD
var tbl_history;
$(document).ready(function () {
    $('#example').DataTable({
        "dom": '<"toolbar">frtip'
    });

    NewOrders.init();

    $('#btn_generate').on('click',function(){


		var date_from = $('#from').val();
		var date_to = $('#to').val();		
		var data = {datefrom:date_from,dateto:date_to};


		$.ajax({
		  url: '/reports/getdeposits',
		  type:"POST",
		  data: data,
		  dataType:'json',
		  success: function(data){
            loadTable(data);
		  }
		});
	});

    tbl_history = $("#example").DataTable();
    $("div.toolbar").html('<a id="btn_add" data-toggle="modal" data-target="#portlet-config" style="margin-top: 30px"  href="javascript:void(0);" class="btn btn-info"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp; Add Deposit</a>');
    $("#btn_generate").click();
});

function loadTable(data) {

    tbl_history.destroy();
    $('#example tbody').html('');

    $('#example tbody').html('');
    var html = "";
    var total = 0;
    $.each(data, function (index, object) {

        html += "<tr>";
        html += "<td>"+object.bank_name+"</td>";
        html += "<td>"+object.branch+"</td>";
        html += "<td>"+object.type+"</td>";
         html += "<td>"+object.account_number+"</td>";
        html += "<td>"+object.cheque_number+"</td>";
        html += "<td>"+object.collection_date+"</td>";
        html += "<td>"+object.deposit_date+"</td>";
        html += "<td>"+object.amount+"</td>";
        html += "</tr>";

    });

    $('#example tbody').append(html);
    tbl_history = $('#example').DataTable({pageLength:25, "dom": '<"toolbar">frtip'});
    $("div.toolbar").html('<a id="btn_add" data-toggle="modal" data-target="#portlet-config" style="margin-top: 30px"  href="javascript:void(0);" class="btn btn-info"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>&nbsp; Add Deposit</a>');
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>