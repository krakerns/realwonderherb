<?php
/**
 * Created by PhpStorm.
 * User: jvjunsay
 * Date: 2/18/17
 * Time: 11:58 PM
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

?>

<div class="row" style="text-align: right;">
    <input type="button" class="btn btn-large btn-info" <?= ($order->is_paid==1)?'disabled':'' ?> value="Pay Now" id="btnPayNow">
    <input type="button" class="btn btn-large btn-success" value="Print" id="prnt">
</div>
<div class="row">
    <div class="col-md-12" id="contentdiv">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo Yii::getAlias('@web'); ?>/images/Logo2withcircle.png" /> <br />
                [Address Here]
            </div>
        </div>
        <div class="row">
            <div class="col-md-10" style="text-align: right">
                Date :
            </div>
            <div class="col-md-2" style="text-align: right">
                <?php
                    $daten = strtotime($order->created);
                    $daten = date("d F Y", $daten);
                    echo $daten;
                ?>
            </div>
            <div class="col-md-10" style="text-align: right">
                Invoice # :
            </div>
            <div class="col-md-2" style="text-align: right">
                <?= Yii::$app->LibraryLoader->leading_zeros($order->id,9) ?>
            </div>

            <div class="col-md-10" style="text-align: right">
                Terms :
            </div>
            <div class="col-md-2" style="text-align: right">
                <?= $order->terms . ' days'  ?>
            </div>
        </div>

        <div style="clear:both;">&nbsp;</div>

        <div class="row">

            <div class="col-md-12">
               <span> <?= $order->customer->fullName;  ?> </span> <br />
                <span> <?= $order->customer->street_address . ' ' . $order->customer->city0->name;  ?> </span><br />
                <span> <?= 'Referrer Name : ' . $order->sponsor->fullName;  ?>
            </div>
        </div>

        <div style="clear:both;"><br /></div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="tbl_items" class="table table-hover table-light">
                        <thead>
                        <tr class="uppercase" style="font-size:10px !important;">
                            <th style="text-align: left;width: 10%;font-weight: normal !important;">
                                Code
                            </th>
                            <th  style="text-align: left;width: 25%;font-weight: normal !important;">
                                Description
                            </th>
                            <th  style="width: 10%;text-align: right;font-weight: normal !important;">
                                Qty
                            </th>
                            <th  style="width: 15%;text-align: right;font-weight: normal !important;">
                                SRP Price
                            </th>
                            <th  style="width: 15%;text-align: right;font-weight: normal !important;">
                                Total
                            </th>

                            <th  style="width: 15%;text-align: right;color:#9e0505;font-weight: normal !important;">
                                Discount
                            </th>

                            <th  style="width: 20%;text-align: right;font-weight: normal !important;">
                                Amount
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                            <?php


                                foreach($order->orderLines as $line){
                                    ?>

                                    <tr style="text-align: right;font-size:10px !important;">
                                        <td style="text-align: left;"><?= $line->product->sku; ?></td>
                                        <td style="text-align: left;"><?= $line->product->name; ?></td>
                                        <td><?= number_format($line->qty,2,".",","); ?></td>
                                        <td><?= number_format($line->srp,2,".",","); ?></td>
                                        <td><?= number_format($line->total,2,".",","); ?></td>
                                        <td><?= number_format($line->discount,2,".",","); ?></td>
                                        <td><?= number_format($line->amount,2,".",","); ?></td>
                                    </tr>

                                    <?php
                                }

                            ?>
                        </tbody>
                        <tfoot>
                        <tr style="border-top:1px solid black !important;margin-top:10px;font-size:11px !important;">
                            <td colspan="4" style="text-align: right">
                                <span style="font-weight:bold;">Total</span>
                                <input type="hidden" name="hdSubTotal" id="hdSubTotal" value="0" />
                            </td>
                            <td style="text-align: right;font-weight:bold;">
                                <span id="main_total"><?= number_format($order->sub_total,2,".",","); ?></span>
                            </td>
                            <td style="text-align: right;font-weight:bold;color:#9e0505;">
                                ( <span id="main_discount"><?= number_format($order->discount,2,".",","); ?></span> )
                                <input type="hidden" name="hdTotalDiscount" id="hdTotalDiscount" value="0" />
                            </td>

                            <td style="text-align: right;font-weight:bold;">
                                <span id="main_amount"><?= number_format($order->grand_total,2,".",","); ?></span>
                                <input type="hidden" name="hdTotal" id="hdTotal" value="0" />
                            </td>
                            <td style="text-align: right;font-weight:bold;">
                                &nbsp;
                            </td>

                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

        <div style="clear:both;"><br /><br /><br /></div>

        <div class="row">

            <div class="col-md-12">
                <table class="no-border" style="width: 100%;">
                    <tr>
                        <td style="width: 50%">
                            <span>Received the above merchandise in good order and condition</span> <br /><br />
                            <p style="border-bottom:1px solid black;">&nbsp;</p>

                        </td>
                        <td style="text-align: right;">

                            <table class="no-border" style="width:100%;">
                                <tr>
                                    <td style="text-align: right;"><span >Grand Total :</span></td>
                                    <td style="text-align: right;"><span ><?= number_format($order->grand_total,2,".",","); ?></span></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;"><span >Mini Subsidy :</span></td>
                                    <td style="text-align: right;"><span ><?= number_format($order->mini_subsidy,2,".",","); ?></span></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;"><span>Amount Due :</span></td>
                                    <td style="text-align: right;"><span><?= number_format($order->amount_due,2,".",","); ?></span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:50%">&nbsp;</td>
                        <td style="text-align:right;">
                            <table class="no-border" style="width:100%;">
                                <?php
                                $payments = \common\models\OrderPayments::findAll(['order_id'=>$order->id,'is_invoice'=>1]);

                                $payment_paid = 0;
                                if(count($payments>0)){
                                    foreach($payments as $p){
                                        $payment_paid+= $p->amount_paid;
                                        ?>
                                        <tr>
                                            <td style="text-align: right;"><span>Payment:</span></td>
                                            <td style="text-align: right;color: red;"><span>( <?= number_format($p->amount_paid,2,".",","); ?> )</span></td>
                                        </tr>
                                    <?php
                                    }
                                }else{
                                    ?>
                                    <tr>
                                        <td style="text-align: right;"><span>Payment:</span></td>
                                        <td style="text-align: right;color: red;"><span>( <?= number_format(0,2,".",","); ?> )</span></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                <tr>
                                    <td style="text-align: right;"><span>Remaining Balance:</span></td>
                                    <td style="text-align: right;"><span> <?= number_format($order->remaining_balance,2,".",","); ?> </span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="clear:both;"><br /><br /></div>

        <div class="row">

            <div class="col-md-12">
                <span>Subsidy Bracket</span> <br />
            </div>

        </div>

        <div style="clear:both;"><br /><br /></div>
        <div class="row" style="display:none;">
            <div class="col-md-7">
                <div class="table-responsive">
                    <table id="tbl_items2" class="table table-hover table-light">
                        <thead>
                        <tr class="uppercase" style="font-weight: normal !important;font-size:10px !important;">
                            <th style="width: 25%;font-weight: normal !important;">
                                Subsidy %
                            </th>

                            <th style="width: 25%;font-weight: normal !important;">
                                This receipt
                            </th>
                            <th style="width: 25%;font-weight: normal !important;">
                                MTD NBD
                            </th>
                            <th style="width: 25%;font-weight: normal !important;">
                                MTD Subsidy
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                            <tr style="font-size:10px !important;">
                                <td><?= $order->mini_subsidy_perc ?> %</td>
                                <td><?= number_format($order->grand_total,2,".",",") ?></td>
                                <td><?= number_format($order->mini_accumulated_sales,2,".",",") ?></td>
                                <td><?= number_format($order->mini_subsidy,2,".",",") ?></td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="table-responsive">
                    <table id="tbl_items4" class="table table-hover table-light">
                        <tbody>
                        <tr style="font-size:10px !important;">
                            <td>Credit Limit</td>
                            <td><?= number_format($order->customer->credit_limit,2,".",",")  ?></td>
                        </tr>
                        <tr style="font-size:10px !important;">
                            <td>Less prev balance</td>
                            <td><?= number_format($order->prev_balance,2,".",",")  ?></td>
                        </tr>

                        <tr style="font-size:10px !important;">
                            <td>Less this invoice</td>
                            <td><?= number_format($order->amount_due,2,".",","); ?></td>
                        </tr>

                        <tr style="font-size:10px !important;">
                            <td>Available credit limit</td>
                            <td><?=  floatval($order->customer->credit_limit) - floatval($order->prev_balance) - floatval($order->amount_due)?></td>
                        </tr>

                        </tbody>
                    </table>
                </div>
        </div>
    </div>
</div>

    <?php $form = ActiveForm::begin([
        'id'=>'add-item-form',
        'action' => '/invoice/payorder',
        'options'=>['enctype'=>'multipart/form-data','class'=>'add-item-form','name'=>'add-item-form'],
    ]); ?>

    <a style="display:none;" id="lnkModal" href='#' data-toggle='modal' data-target='#portlet-config'>Test</a>
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h2 class="modal-title">Payment</h2>
                </div>
                <div class="modal-body">
                    <div class="products-form">

                        <h3 id="product_n"></h3>
                        <input type="hidden" name="hdOrderId" value="<?= $order->id ?>">
                        <input type="hidden" name="hdGrandTotal" value="<?= $order->remaining_balance ?>">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">PAYMENT TYPE:</label>
                                    <?= Html::radioList('payment_type',['cash'], ArrayHelper::map([['id'=>'cash','name'=>'Cash'],
                                        ['id'=>'card','name'=>'Card'],
                                        ['id'=>'cheque','name'=>'Cheque']
                                    ], 'id', 'name'),['id'=>'payment_type']) ?>
                                </div>
                            </div>

                            <div class="col-md-12" id="divCheque" style="display:none;">
                                <div class="form-group">
                                    <label class="control-label">BANK NAME:</label>
                                    <input type="text" class="form-control" id="txtBankName" name="txtBankName" value=""   />
                                </div>

                                <div class="form-group">
                                    <label class="control-label">BANK NUMBER:</label>
                                    <input type="text" class="form-control" id="txtBankNumber" name="txtBankNumber" value=""   />
                                </div>

                                <div class="form-group">
                                    <label class="control-label">CHEQUE NUMBER:</label>
                                    <input type="text" class="form-control" id="txtChequeNumber" name="txtChequeNumber" value=""   />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">TOTAL:</label>
                                    <input id="txtTotal" disabled="disabled" type="text" class="form-control" style="color:#9e0505;height: 100px;width: 98%;font-size: 48px;text-align: right;" value="<?= number_format($order->remaining_balance,2,".",","); ?>" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">AMOUNT TO PAY:</label>
                                    <input id="txtAmount" name="txtAmount" type="text" onfocus="this.select();" class="form-control numeric" style="height: 100px;width:98%;font-size: 48px;text-align: right;" value="0.00" />
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" id="btnProceed" class="btn btn-primary" value="Proceed"/>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <?php ActiveForm::end(); ?>
<?php
$scrpt = <<<EOD
$(document).ready(function () {

    $('#prnt').click(function(){
        var divContents = $("#contentdiv").html();
        var printWindow = window.open('', '', 'height=600,width=800');
        printWindow.document.write('<html><head><title></title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();

    });

     $('#btnPayNow').click(function(){
        $('#lnkModal').click();
     });

     $('input[name="payment_type"]').change(function(){
        $('#txtBankName').val('');
        $('#txtBankNumber').val('');
        $('#txtChequeNumber').val('');

        if($(this).val() == 'cheque' ){
            $('#divCheque').show();
        }else{
            $('#divCheque').hide();
        }
    });


});
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>