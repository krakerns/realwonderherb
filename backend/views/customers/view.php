<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
?>
<div class="customers-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute'=>'photo_url',
                'value'=>(Yii::getAlias('@web') . '/' . 'uploads/' . $model->photo_url),
                'format' => ['image',['width'=>'230','height'=>'200']],
            ],
            'sponsor.fullName',
            'fullName',
            'business_name',
            'last_name',
            'contact_number',
            'email:email',
            'street_address:ntext',
            'city',
            'state',
            'zipcode',
            'tax_number',
            'ssn',
            'date_of_birth',

        ],
    ]) ?>

</div>
