<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customers-form">

    <?php $form = ActiveForm::begin(); ?>

    <?=

    $form->field($model, 'image')->widget(FileInput::classname(), [
        'options'=>['accept'=>'image/*'],    'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png']]
        ]) ?>

    <?= $form->field($model, 'sponsor_id')->dropDownList(Yii::$app->LibraryLoader->getSponsors('-Select Sponsor-'),['id'=>'sponsor_id']) ?>
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'contact_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'street_address')->textarea(['rows' => 6]) ?>
     <?= $form->field($model, 'city')->dropDownList(Yii::$app->LibraryLoader->getCities('-Select City-'),['id'=>'city']) ?>
     <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>    
    <?= $form->field($model, 'zipcode')->textInput() ?>    <?= $form->field($model, 'tax_number')->textInput() ?>
    <?= $form->field($model, 'credit_limit')->textInput() ?>
    
      
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
