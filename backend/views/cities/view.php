<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Cities */
?>
<div class="cities-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'province_id',
        ],
    ]) ?>

</div>
