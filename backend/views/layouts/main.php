<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => '<img src="'.Yii::getAlias('@web').'/images/RealWonderHerb_Logo_vector--01.png" class="img-logo" alt="loading-img">',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-backcolor navbar-fixed-top',
        ],
    ]);
    $menuItems = [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Members','items'=>[
            ['label' => 'PE (Partner Entrepreneur', 'url' => ['/customers?CustomersSearch[is_mini]=0']],
            ['label' => 'MCO (Mini Center Operator)', 'url' => ['/customers?CustomersSearch[is_mini]=1']],
            ['label' => 'Employee', 'url' => ['/customers?CustomersSearch[is_mini]=2']],
            ['label' => 'Genealogy', 'url' => ['/reports/genealogy']],
            ['label' => 'Incentives / Referrals', 'url' => ['/reports/passiveincome']],
        ]],
        ['label' => 'Inventory','items'=>[
            ['label' => 'Stocks On Hand', 'url' => ['/products/inventory']],
            ['label' => 'Categories', 'url' => ['/category']],
            ['label' => 'Products', 'url' => ['/products?ProdSearch[is_bundle]=0']],
            ['label' => 'Bundles / Promotions', 'url' => ['/products?ProdSearch[is_bundle]=1']],
        ]],
        ['label' => 'Transactions','items'=>[
            ['label' => 'Point Of Sale', 'url' => ['/orders/pos']],
            ['label' => 'New Invoice', 'url' => ['/invoice/newinvoice']],
            ['label' => 'Sales Receipts', 'url' => ['/orders/invoices']],
        ]],
        ['label' => 'Payments','items'=>[
            ['label' => 'Invoices', 'url' => ['/invoice/invoices']],
            ['label' => 'Payments History', 'url' => ['/invoice/paymenthistory']],
            ['label' => 'Payments Report', 'url' => ['/invoice/paymentreport']],
        ]],
        ['label' => 'Reports','items'=>[
            ['label' => 'Collections', 'url' => ['/reports/collections']],
            ['label' => 'Daily Collections', 'url' => ['/reports/dailycollection']],
            ['label' => 'Deposits', 'url' => ['/reports/deposits']],
            ['label' => 'Deposits Report', 'url' => ['/reports/depositsreport']],
            ['label' => 'Subsidy Report', 'url' => ['/reports/subsidy']],
            ['label' => 'Sales Report (Branch)', 'url' => ['/reports/salesbranch']],
            ['label' => 'Sales Report (Group)', 'url' => ['/reports/salesgroup']],
            ['label' => 'Sales Report (Dealer)', 'url' => ['/reports/salesdealer']],
            ['label' => 'New Sign Ups', 'url' => ['/reports/signups']],
            ['label' => 'Stock Movement', 'url' => ['/reports/stockmovement']],
            ['label' => 'Subsidy Refund', 'url' => ['/reports/subsidyrefund']],
        ]],
    ];

    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->username . ')',
                ['class' => 'btn btn-link white-font']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
    <div style="clear:both;"><br /></div>
    <div class="container">
        <br />
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy;Real Wonder Herb <?= date('Y') ?></p>


    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
