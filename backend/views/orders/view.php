<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */
?>
<div class="orders-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'sponsor_id',
            'grand_total',
            'mini_subsidy',
            'discount',
            'amount_due',
            'created_by',
            'created',
            'sub_total',
            'mini_subsidy_perc',
            'mini_accumulated_sales',
        ],
    ]) ?>

</div>
