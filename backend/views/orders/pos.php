<?php
/* @var $this yii\web\View */
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<?php $form = ActiveForm::begin([
    'id'=>'add-item-form',
    'action' => '/orders/saveorders',
    'options'=>['enctype'=>'multipart/form-data','class'=>'add-item-form','name'=>'add-item-form'],
]); ?>

<div class="row">
    <h1>Point Of Sale</h1>
    <hr />
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Customer</label>
            <?=

                    Typeahead::widget([
                        'name' => 'country',
                        'options' => ['placeholder' => 'Select Customer'],
                        'pluginOptions' => ['highlight'=>true],
                        'dataset' => [
                            [
                                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                                'display' => 'full_name',
                                 'remote' => [
                                    'url' => Url::to(['/customers/jsonlist']) . '?q=%QUERY',
                                    'wildcard' => '%QUERY'
                                ]
                            ]
                        ],
                        'pluginEvents'=>[
                            "typeahead:selected" => "function(obj, item) {
                            console.log(item);
                                $('#sponsor').val(item.sponsor_name);
                                $('#hdSponsorId').val(item.sponsor_id);
                                $('#hdCustomerId').val(item.id);
                                $('#hdSubsidyPerc').val(item.purchase_percentage);
                                $('#hdAccumulatedSales').val(item.sales);
                            }",
                        ]
                    ]);
             ?>
        </div>

    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Sponsor</label>
            <?=Html::input('text','sponsor','',['class'=>'form-control','disabled'=>'disabled','id'=>'sponsor'])?>
            <input type="hidden" name="hdSponsorId" id="hdSponsorId" value="0" />
            <input type="hidden" name="hdAccumulatedSales" id="hdAccumulatedSales" value="0" />
            <input type="hidden" name="hdSubsidyPerc" id="hdSubsidyPerc" value="0" />
            <input type="hidden" name="hdCustomerId" id="hdCustomerId" value="0" />
            <input type="hidden" name="hdLineCount" id="hdLineCount" value="0" />
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Date</label>
            <?=Html::input('text','date',date("m.d.y"),['class'=>'form-control','disabled'=>'disabled'])?>
        </div>
    </div>

</div>

<hr />

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Select Product</label>
            <?=

            Typeahead::widget([
                'name' => 'product',
                'options' => ['placeholder' => 'Enter SKU / Name'],
                'pluginOptions' => ['highlight'=>true],
                'dataset' => [
                    [
                        'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('value')",
                        'display' => 'sku_name',
                        'remote' => [
                            'url' => Url::to(['/products/jsonlist']) . '?q=%QUERY',
                            'wildcard' => '%QUERY'
                        ]
                    ]
                ],
                'pluginEvents'=>[
                    "typeahead:selected" => "function(obj, item) {
                        NewOrders.addItems(item);
                    }",
                ]
            ]);
            ?>
        </div>
    </div>
</div>

<div class="Row">
    <div class="col-md-12">
        <div class="table-responsive">
            <table id="tbl_items" class="table table-hover table-light">
                <thead>
                    <tr class="uppercase">
                        <th style="width: 10%;">
                            Code
                        </th>
                        <th  style="width: 25%;">
                           Description
                        </th>
                        <th  style="width: 10%;text-align: right;">
                            Qty
                        </th>
                        <th  style="width: 15%;text-align: right;">
                            SRP Price
                        </th>
                        <th  style="width: 15%;text-align: right;">
                            Total
                        </th>

                        <th  style="width: 15%;text-align: right;color:#9e0505;">
                            Discount
                        </th>

                        <th  style="width: 15%;text-align: right;">
                            Amount
                        </th>
                        <th  style="width: 5%;">
                            &nbsp;
                        </th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
                <tfoot>
                    <tr style="border-top:1px;">
                        <td colspan="4" style="text-align: right">
                            <span style="font-weight:bold;">Total</span>
                            <input type="hidden" name="hdSubTotal" id="hdSubTotal" value="0" />
                        </td>
                        <td style="text-align: right;font-weight:bold;">
                            <span id="main_total">0.00</span>
                        </td>
                        <td style="text-align: right;font-weight:bold;color:#9e0505;">
                            ( <span id="main_discount">0.00</span> )
                            <input type="hidden" name="hdTotalDiscount" id="hdTotalDiscount" value="0" />
                        </td>

                        <td style="text-align: right;font-weight:bold;">
                            <span id="main_amount">0.00</span>
                            <input type="hidden" name="hdTotal" id="hdTotal" value="0" />
                        </td>
                        <td style="text-align: right;font-weight:bold;">
                           &nbsp;
                        </td>

                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>

<div style="clear: both;">&nbsp;</div>
<div class="row" style="margin-top:20px;">
    <div class="col-md-9" style="text-align: right;border-top:1px; ">
        <h4 style="font-weight:bold;">Grand Total : </h4>
    </div>
    <div class="col-md-3" style="text-align: right;border-top:1px; ">
        <h4 id="hTotal" style="font-weight:bold;">0.00</h4>
        <input type="hidden" name="hdGrandTotal" id="hdGrandTotal" value="0" />
    </div>
</div>

<div class="row" style="display: none;">
    <div class="col-md-9" style="text-align: right;border-top:1px; ">
        <h4 style="font-weight:bold;color:#9e0505;">Mini Subsidy : </h4>
    </div>
    <div class="col-md-3" style="text-align: right;border-top:1px; ">
        <h4 id="hMini" style="font-weight:bold;color:#9e0505;">0.00</h4>
        <input type="hidden" name="hdMiniSubsidy" id="hdMiniSubsidy" value="0" />
        <input type="hidden" name="hdMiniSubsidyDiscount" id="hdMiniSubsidyDiscount" value="0" />
    </div>
</div>

<div class="row">
    <div class="col-md-9" style="text-align: right;border-top:1px; ">
        <h4 style="font-weight:bold;">Amount Due : </h4>

    </div>
    <div class="col-md-3" style="text-align: right;border-top:1px; ">
        <h4 id="hAmount" style="font-weight:bold;">0.00</h4>
        <input type="hidden" name="hdAmountDue"  id="hdAmountDue" value="0" />
    </div>
</div>

<div style="clear: both;">&nbsp;</div>

<div class="row" style="margin-top:20px;">
    <div class="col-md-12" style="text-align: right;border-top:1px; ">
        <input type="button" id="btnPayNow"  class="btn btn-primary" value="Pay Now" />
    </div>
</div>

<a style="display:none;" id="lnkModal" href='#' data-toggle='modal' data-target='#portlet-config'>Test</a>
<div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h2 class="modal-title">Payment</h2>
            </div>
            <div class="modal-body">
                <div class="products-form">

                    <h3 id="product_n"></h3>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">PAYMENT TYPE:</label>
                                <?= Html::radioList('payment_type',['cash'], ArrayHelper::map([['id'=>'cash','name'=>'Cash'],
                                    ['id'=>'card','name'=>'Card'],
                                    ['id'=>'cheque','name'=>'Cheque']
                                ], 'id', 'name'),['id'=>'payment_type']) ?>
                            </div>
                        </div>

                        <div class="col-md-12" id="divCheque" style="display:none;">
                            <div class="form-group">
                                <label class="control-label">BANK NAME:</label>
                                <input type="text" class="form-control" id="txtBankName" name="txtBankName" value=""   />
                            </div>

                            <div class="form-group">
                                <label class="control-label">BANK NUMBER:</label>
                                <input type="text" class="form-control" id="txtBankNumber" name="txtBankNumber" value=""   />
                            </div>

                            <div class="form-group">
                                <label class="control-label">CHEQUE NUMBER:</label>
                                <input type="text" class="form-control" id="txtChequeNumber" name="txtChequeNumber" value=""   />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">TOTAL:</label>
                                <input id="txtTotal" disabled="disabled" type="text" class="form-control" style="color:#9e0505;height: 100px;width: 98%px;font-size: 48px;text-align: right;" value="0.00" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">AMOUNT TO PAY:</label>
                                <input id="txtAmount" name="txtAmount" type="text" onfocus="this.select();" class="form-control numeric" style="height: 100px;width:98%;font-size: 48px;text-align: right;" value="0.00" />
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">CHANGE:</label>
                                <input id="txtChange" name="txtChange" readonly="true" type="text" class="form-control" style="height: 100px;width:98%;font-size: 48px;text-align: right;" value="0.00" />
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <input type="submit" id="btnProceed" class="btn btn-primary" value="Proceed"/>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php ActiveForm::end(); ?>


<?php
$scrpt = <<<EOD
$(document).ready(function () {

    NewOrders.init();
    $('#btnPayNow').prop('disabled',true);
});
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>