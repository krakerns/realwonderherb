<?php
/**
 * Created by PhpStorm.
 * User: jvjunsay
 * Date: 2/18/17
 * Time: 11:58 PM
 */

?>

<div class="row" style="text-align: right;">
    <input type="button" class="btn btn-success" value="Print" id="prnt">
</div>
<div class="row">
    <div class="col-md-12" id="contentdiv">
        <div class="row">
            <div class="col-md-12">
                <img src="<?php echo Yii::getAlias('@web'); ?>/images/Logo2withcircle.png" /> <br />
                [Address Here]
            </div>
        </div>
        <div class="row">
            <div class="col-md-10" style="text-align: right">
                Date :
            </div>
            <div class="col-md-2" style="text-align: right">
                <?php
                    $daten = strtotime($order->created);
                    $daten = date("d F Y", $daten);
                    echo $daten;
                ?>
            </div>
            <div class="col-md-10" style="text-align: right">
                Ref # :
            </div>
            <div class="col-md-2" style="text-align: right">
                <?= Yii::$app->LibraryLoader->leading_zeros($order->id,9) ?>
            </div>

        </div>

        <div style="clear:both;">&nbsp;</div>

        <div class="row">

            <div class="col-md-12">
               <span> <?= $order->customer->fullName;  ?> </span> <br />
                <span> <?= $order->customer->street_address . ' ' . $order->customer->city0->name;  ?> </span><br />
                <span> <?= 'Referrer Name : ' . $order->sponsor->fullName;  ?>
            </div>
        </div>

        <div style="clear:both;"><br /></div>

        <div class="row">
            <div class="col-md-12">
                <div class="table-responsive">
                    <table id="tbl_items" class="table table-hover table-light">
                        <thead>
                        <tr class="uppercase" style="font-size:10px !important;">
                            <th style="text-align: left;width: 10%;font-weight: normal !important;">
                                Code
                            </th>
                            <th  style="text-align: left;width: 25%;font-weight: normal !important;">
                                Description
                            </th>
                            <th  style="width: 10%;text-align: right;font-weight: normal !important;">
                                Qty
                            </th>
                            <th  style="width: 15%;text-align: right;font-weight: normal !important;">
                                SRP Price
                            </th>
                            <th  style="width: 15%;text-align: right;font-weight: normal !important;">
                                Total
                            </th>

                            <th  style="width: 15%;text-align: right;color:#9e0505;font-weight: normal !important;">
                                Discount
                            </th>

                            <th  style="width: 20%;text-align: right;font-weight: normal !important;">
                                Amount
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                            <?php


                                foreach($order->orderLines as $line){
                                    ?>

                                    <tr style="text-align: right;font-size:10px !important;">
                                        <td style="text-align: left;"><?= $line->product->sku; ?></td>
                                        <td style="text-align: left;"><?= $line->product->name; ?></td>
                                        <td><?= number_format($line->qty,2,",","."); ?></td>
                                        <td><?= number_format($line->srp,2,",","."); ?></td>
                                        <td><?= number_format($line->total,2,",","."); ?></td>
                                        <td><?= number_format($line->discount,2,",","."); ?></td>
                                        <td><?= number_format($line->amount,2,",","."); ?></td>
                                    </tr>

                                    <?php
                                }

                            ?>
                        </tbody>
                        <tfoot>
                        <tr style="border-top:1px solid black !important;margin-top:10px;font-size:11px !important;">
                            <td colspan="4" style="text-align: right">
                                <span style="font-weight:bold;">Total</span>
                                <input type="hidden" name="hdSubTotal" id="hdSubTotal" value="0" />
                            </td>
                            <td style="text-align: right;font-weight:bold;">
                                <span id="main_total"><?= number_format($order->sub_total,2,",","."); ?></span>
                            </td>
                            <td style="text-align: right;font-weight:bold;color:#9e0505;">
                                ( <span id="main_discount"><?= number_format($order->discount,2,",","."); ?></span> )
                                <input type="hidden" name="hdTotalDiscount" id="hdTotalDiscount" value="0" />
                            </td>

                            <td style="text-align: right;font-weight:bold;">
                                <span id="main_amount"><?= number_format($order->grand_total,2,",","."); ?></span>
                                <input type="hidden" name="hdTotal" id="hdTotal" value="0" />
                            </td>
                            <td style="text-align: right;font-weight:bold;">
                                &nbsp;
                            </td>

                        </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>

        <div style="clear:both;"><br /><br /><br /></div>

        <div class="row">

            <div class="col-md-12">
                <table class="no-border" style="width: 100%;">
                    <tr>
                        <td style="width: 50%">
                            <span>Received the above merchandise in good order and condition</span> <br /><br />
                            <p style="border-bottom:1px solid black;">&nbsp;</p>

                        </td>
                        <td style="text-align: right;">

                            <table class="no-border" style="width:100%;">
                                <tr>
                                    <td style="text-align: right;"><span >Grand Total :</span></td>
                                    <td style="text-align: right;"><span ><?= number_format($order->grand_total,2,",","."); ?></span></td>
                                </tr>
                                <tr style="display: none;">
                                    <td style="text-align: right;"><span >Mini Subsidy :</span></td>
                                    <td style="text-align: right;"><span ><?= number_format($order->mini_subsidy,2,",","."); ?></span></td>
                                </tr>
                                <tr>
                                    <td style="text-align: right;"><span>Amount Due :</span></td>
                                    <td style="text-align: right;"><span><?= number_format($order->amount_due,2,",","."); ?></span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div style="clear:both;"><br /><br /></div>

        <div class="row" style="display: none;">

            <div class="col-md-12">
                <span>Subsidy Bracket</span> <br />
            </div>

        </div>

        <div style="clear:both;"><br /><br /></div>
        <div class="row" style="display:none;">
            <div class="col-md-7">
                <div class="table-responsive">
                    <table id="tbl_items2" class="table table-hover table-light">
                        <thead>
                        <tr class="uppercase" style="font-weight: normal !important;font-size:10px !important;">
                            <th style="width: 25%;font-weight: normal !important;">
                                Subsidy %
                            </th>

                            <th style="width: 25%;font-weight: normal !important;">
                                This receipt
                            </th>
                            <th style="width: 25%;font-weight: normal !important;">
                                MTD NBD
                            </th>
                            <th style="width: 25%;font-weight: normal !important;">
                                MTD Subsidy
                            </th>

                        </tr>
                        </thead>
                        <tbody>
                            <tr style="font-size:10px !important;">
                                <td><?= $order->mini_subsidy_perc ?> %</td>
                                <td><?= number_format($order->grand_total,2,",",".") ?></td>
                                <td><?= number_format($order->mini_accumulated_sales,2,",",".") ?></td>
                                <td><?= number_format($order->mini_subsidy,2,",",".") ?></td>
                            </tr>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>


<?php
$scrpt = <<<EOD
$(document).ready(function () {

    $('#prnt').click(function(){
        var divContents = $("#contentdiv").html();
        var printWindow = window.open('', '', 'height=600,width=800');
        printWindow.document.write('<html><head><title></title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');
        printWindow.document.close();
        printWindow.print();

    });

});
EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>