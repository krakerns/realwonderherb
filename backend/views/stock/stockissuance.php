<?php
/* @var $this yii\web\View */
use kartik\typeahead\TypeaheadBasic;
use kartik\typeahead\Typeahead;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

$this->registerJsFile('@web/js/orders.js',['depends' => [\yii\web\JqueryAsset::className()]]);
?>

<?php $form = ActiveForm::begin([
    'id'=>'add-item-form',
    'action' => '#',
    'options'=>['enctype'=>'multipart/form-data','class'=>'add-item-form','name'=>'add-item-form'],
]); ?>

<div class="row">
    <h1>Stock Issuance</h1>
    <hr />

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Date</label>
                <?=Html::input('text','date',date("m.d.y"),['class'=>'form-control','disabled'=>'disabled'])?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Requested By</label>
                <?=Html::input('text','date',date("m.d.y"),['class'=>'form-control','disabled'=>'disabled'])?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Approved By</label>
                <?=Html::input('text','date',date("m.d.y"),['class'=>'form-control','disabled'=>'disabled'])?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Reason</label>
                <?=Html::textarea('reason','',['class'=>'form-control']) ?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Encoded By</label>
                <?=Html::input('text','date',date("m.d.y"),['class'=>'form-control','disabled'=>'disabled'])?>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Received By</label>
                <?=Html::input('text','date',date("m.d.y"),['class'=>'form-control','disabled'=>'disabled'])?>
            </div>
        </div>
    </div>

    <hr />

    <div class="Row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table id="tbl_items" class="table table-hover table-light">
                    <thead>
                    <tr class="uppercase">
                        <th style="width: 10%;">
                            Code
                        </th>
                        <th  style="width: 25%;">
                            Description
                        </th>
                        <th  style="width: 10%;text-align: right;">
                            Qty
                        </th>
                        <th  style="width: 15%;text-align: right;">
                            Amount
                        </th>
                        <th  style="width: 15%;text-align: right;">
                            Total
                        </th>

                        <th  style="width: 5%;">
                            &nbsp;
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>

                </table>
            </div>
        </div>
    </div>
</div>


<?php ActiveForm::end(); ?>


<?php

?>