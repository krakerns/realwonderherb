<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Category */
?>
<div class="category-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cat_name',
            'description:ntext',

        ],
    ]) ?>

</div>
