<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
?>
<div class="products-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'categoryName',
            'sku',
            'name',
            'description:ntext',
            'long_description:ntext',
            'wholesale_price',
            'retail_price',
            'volume',
            'weight',
            'image',
            'reverse_image',
            'thumbnail_image',
            'active',
            'product_type_id',
            'is_bundle',
            'reorder_number',

        ],
    ]) ?>

</div>
