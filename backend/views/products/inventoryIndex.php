<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products Inventory';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="products-index">
    <div id="ajaxCrudDatatable">
        <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'pjax'=>true,
            'columns' => [
                'sku',
                'name',
                'quantity',

                [
                    'class' => '\kartik\grid\ActionColumn',
                    'template'      => '{active}',
                    'buttons'       => [
                        "active" => function ($url, $model) {
                                $options = [
                                    'title' => Yii::t('yii', 'See in frontend'),
                                    'aria-label' => Yii::t('yii', 'See'),
                                    'data-pjax' => '1',
                                ];
                                return "<a style='cursor:pointer;' onclick='clickme(\"".$model['name']."\",\"".$model['pid']."\",\"".$model['sku']."\",\"".$model['quantity']."\");'>Add Quantity</a>";
                            },
                    ],
                    'urlCreator'    => function ($action, $model, $key, $index) {
                            return \yii\helpers\Url::to(["products/update", "id" => $model['pid']]);
                        },

            ]],

            'striped' => true,
            'condensed' => true,
            'responsive' => true,
            'panel' => [
                'type' => 'herb',
                'heading' => '<i class="glyphicon glyphicon-list"></i> Inventory listing',
                'before'=>'<em>* Resize table columns just like a spreadsheet by dragging the column edges.</em>',
                '<div class="clearfix"></div>',
            ]
        ])?>
    </div>
    <a style="display:none;" id="lnkModal" href='#' data-toggle='modal' data-target='#portlet-config'>Test</a>
    <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <?php $form = ActiveForm::begin([
                    'id'=>'buy-credit-form',
                    'action' => '/products/inventoryupdate',
                    'options'=>['enctype'=>'multipart/form-data','class'=>'buy-credit-form'],
                ]); ?>
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Inventory</h4>
                </div>
                <div class="modal-body">
                    <div class="products-form">

                        <h3 id="product_n"></h3>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Quantity</label>
                                    <input type="hidden" id="product_id" name="product_id" value="" />
                                    <input type="hidden" id="old_qty" name="old_qty" value="0" />
                                    <input maxlength="16" autocomplete="off" type="text" placeholder="0.00" class="form-control" name="quantity" id="quantity">
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Add to Inventory"/>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

</div>

<?php

$scrpt = <<<EOD
$(document).ready(function () {

});

function clickme(name,pid,sku,qty){
    if(!qty) qty =0;

    $("#old_qty").val(qty);
    $("#product_id").val(pid);
    $("#product_n").html(name);
    $("#lnkModal").click(); }

EOD;

$this->registerJs($scrpt, $this::POS_END, 'init-corejs');

?>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>


