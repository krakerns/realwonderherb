/**
 * Created by jvjunsay on 2/16/17.
 */

var NewOrders = function(){
    var init = function(){

        $('#tbl_items').on('blur','.cls_qty',function(){
            var id = $(this).attr('id').substring(3);
            handleOrders.computeLine(id);
        });


        $('#tbl_items').on('keydown','.cls_qty', function(event) {
            if ( event.keyCode == 46 || event.keyCode == 8 ) {
            }else if(event.keyCode == 13){
                var id = $(this).attr('id').substring(3);
                handleOrders.computeLine(id);
                event.preventDefault();
            }
            else {
                if (event.keyCode < 95) {
                    if (event.keyCode < 48 || event.keyCode > 57 ) {
                        event.preventDefault();
                    }
                } else {
                    if (event.keyCode < 96 || event.keyCode > 105 ) {
                        event.preventDefault();
                    }
                }
            }
        });

        $('#portlet-config').on('keydown','.numeric', function(event) {
            if ( event.keyCode == 46 || event.keyCode == 8 ) {

            }else if(event.keyCode == 13){
                var tot = parseFloat($('#txtTotal').val());
                var amt = parseFloat($('#txtAmount').val());

                $('#txtChange').val((tot-amt).toFixed(2))
                event.preventDefault();
            }
            else {
                if (event.keyCode < 95) {
                    if (event.keyCode < 48 || event.keyCode > 57 ) {
                        event.preventDefault();
                    }
                } else {
                    if (event.keyCode < 96 || event.keyCode > 105 ) {
                        event.preventDefault();
                    }
                }
            }
        });

        $('#tbl_items').on('click','.btn-remove',function(){
           $(this).parent().parent().remove();
            handleOrders.computeAmount();
        });
        $('#txtAmount').change(function(){
            var tot = parseFloat($('#txtTotal').val());
            var amt = parseFloat($('#txtAmount').val());

            var tot_amt = tot-amt;
            $('#txtChange').val((tot-amt).toFixed(2))

        });

        $('#btnPayNow').click(function(){
            $('#txtAmount').val('0.00');
            $('#txtChange').val('0.00');

            $('#lnkModal').click();
        });

        $('input[name="payment_type"]').change(function(){
            $('#txtBankName').val('');
            $('#txtBankNumber').val('');
            $('#txtChequeNumber').val('');

            if($(this).val() == 'cheque' ){
                $('#divCheque').show();
            }else{
                $('#divCheque').hide();
            }
        });
    };

    var handleOrders = {
        itemsCount:1,
        addItems:function(product){

            var hd = $('<input type="hidden" name="hd_pid'+handleOrders.itemsCount+'" id="hd_pid'+handleOrders.itemsCount+'" value="'+product.id+'" />' +
                '<input type="hidden" name="hd_subtotal'+handleOrders.itemsCount+'" id="hd_subtotal'+handleOrders.itemsCount+'" value="0" />' +
                '<input type="hidden" name="hd_discount'+handleOrders.itemsCount+'" id="hd_discount'+handleOrders.itemsCount+'" value="0" />' +
                '<input type="hidden" name="hd_srp'+handleOrders.itemsCount+'" id="hd_srp'+handleOrders.itemsCount+'" value="'+product.retail_price+'" />' +
                '<input type="hidden" name="hd_total'+handleOrders.itemsCount+'" id="hd_total'+handleOrders.itemsCount+'" value="0" />');
            var tr = $('<tr id="'+handleOrders.itemsCount+'"></tr>');
            tr.append(hd);
            tr.append('<td>'+product.sku+'<input type="hidden" id="hd__id'+handleOrders.itemsCount+'" name="hd__id'+handleOrders.itemsCount+'" value="'+product.id+'"/> </td>');
            tr.append('<td>'+product.name+'</td>');
            tr.append('<td style="text-align: right;"><input type="text" onfocus="this.select();" style="text-align: right;width:50% !important;" class="cls_qty" name="qty'+handleOrders.itemsCount+'" id="qty'+handleOrders.itemsCount+'" value="0"/></td>');
            tr.append('<td style="text-align: right;"><span class="cls_retail" id="spn_retail'+handleOrders.itemsCount+'">'+product.retail_price+'</span><input type="hidden" id="hd_retail'+handleOrders.itemsCount+'" name="hd_retail'+handleOrders.itemsCount+'" value="'+product.retail_price+'"/></td>');
            tr.append('<td style="text-align: right;"><span class="cls_total" id="spn_total'+handleOrders.itemsCount+'">0</span></td>');
            tr.append('<td style="text-align: right;color:#9e0505;">( <span class="cls_discount" id="spn_discount'+handleOrders.itemsCount+'">0</span> )</td>');
            tr.append('<td style="text-align: right;font-weight: bold;"><span class="cls_amount" id="spn_amount'+handleOrders.itemsCount+'">0</span></td>');
            tr.append('<td><a class="btn default btn-xs btn-remove  btn-danger" href="javascript:;"><i class="glyphicon glyphicon-remove"></i></a></td>');
            $('#tbl_items tbody').append(tr);
            $('#tbl_items tbody').append(hd);
            $('#w1').val('');
            handleOrders.itemsCount++;
            $('#hdLineCount').val(handleOrders.itemsCount);

        },
        computeLine:function(id){
            var retail = $('#spn_retail' + id).html();
            var qty = $('#qty' + id).val();

            var total = (parseFloat(retail) * parseFloat(qty));
            var discount = total * .30;
            var amount = total - discount;
            $('#spn_total'+id).html(total.toFixed(2));
            $('#hd_subtotal' + id).val(total.toFixed(2));

            $('#spn_discount'+id).html(discount.toFixed(2));
            $('#hd_discount' + id).val(discount.toFixed(2));

            $('#spn_amount'+id).html(amount.toFixed(2));
            $('#hd_total' + id).val(amount.toFixed(2));

            handleOrders.computeAmount();
        },
        computeAmount:function(){
            var total = 0;
            var discount=0;
            var amount =0;

            $('.cls_total').each(function(index){
                total += parseFloat($(this).html());
            });
            $('#main_total').html(total.toFixed(2));
            $('#hdSubTotal').val(total.toFixed(2))

            $('.cls_discount').each(function(index){
                discount += parseFloat($(this).html());
            });
            $('#main_discount').html(discount.toFixed(2));
            $('#hdTotalDiscount').val(discount.toFixed(2));

            $('.cls_amount').each(function(index){
                amount += parseFloat($(this).html());
            });
            $('#main_amount').html(amount.toFixed(2));
            $('#hd_total').val(amount.toFixed(2));

            handleOrders.computeGrandAmount();
        },

        computeGrandAmount:function(){
            var total = $('#main_amount').html();
            var subsidy_perc = parseFloat($('#hdSubsidyPerc').val()) / 100;
            var subsidy =  parseFloat(total) * subsidy_perc;

            var grandTotal = parseFloat(total) - subsidy;

            $('#hTotal').html(total);
            $('#hdGrandTotal').val(total);

            $('#hMini').html(subsidy.toFixed(2));
            $('#hdMiniSubsidy').val(subsidy.toFixed(2));
            $('#hdMiniSubsidyDiscount').val(subsidy.toFixed(2));

            $('#hAmount').html(grandTotal.toFixed(2));
            $('#hdAmountDue').val(grandTotal.toFixed(2));

            $('#txtTotal').val(grandTotal.toFixed(2));

            $('#btnPayNow').prop('disabled',(grandTotal>0)?false:true);
        },
        removeRow:function(){

        },
        loadOrders:function(data){

        }

    }


    return {
        init:function(){
            init();
        },
        addItems:function(items){
            handleOrders.addItems(items);
        }

    }
}();


